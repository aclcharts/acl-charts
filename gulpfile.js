// This module is needed for mocha tests
// running properly when written in es6
require('babel-register');

const gulp = require('gulp'),
    path = require('path'),
    webpack = require('webpack-stream'),
    del = require('del'),
    $ = require('gulp-load-plugins')();


const root = path.resolve(__dirname);

const dir = {
    src: `${root}/src/`,
    js: `${root}/src/js/`,
    tmp: '${root}/build/tmp/',
    html: `${root}/src/html/`,
    sass: `${root}/src/sass/`,
    test: `${root}/src/test/`,
    build: `${root}/build/`,
    report: `${root}/build/report/`,
    coverage: `${root}/build/coverage/`,
    todo: `${root}/build/todo/`,
    doc: `${root}/build/documentation/`
};

const tasks = {
    js: {
        webpack: {
            options: {
                entry: `${dir.js}main.js`,
                output: {
                    path: dir.build,
                    filename: 'bundle.js'
                },
                module: {
                    loaders: [
                        {loader: 'babel-loader'}
                    ]
                },
                devtool: 'source-map'
            }
        },
        source: `${dir.js}/**/*.js`
    },
    mocha: {
        options: {
            reporter: 'mochawesome'
        },
        source: `${dir.js}/**/*.js`,
        sourceTest: `${dir.test}/specs/**/*.spec.js`,
        reportDir: dir.report,
        tempDir: dir.tmp
    },
    istanbul: {
        options: {
            dir: dir.coverage
        }
    },
    todo: {
        source: `${dir.src}/**/*`,
        destination: dir.todo
    },
    esdoc: {
        options: {
            destination: dir.doc
        },
        source: dir.js
    },
    css: {
        source: `${dir.sass}/**/*.scss`,
        concatenate: `bundle.css`,
        destination: dir.build
    },
    clean: {
        css: `${dir.build}/*.css`,
        js: `${dir.build}/*.js`,
        all: `${dir.build}/*`
    },
    webserver: {
        root: dir.build,
        port: 8888,
        host: 'localhost',
        livereload: true
    }
};

gulp.task('default', ['help'], () => {});


gulp.task('help', () => {
    $.util.log(
        `
        ACL Charts develop environment tasks:

        Build:
          build         - Build all assets. For javascript Webpack is used. Also run html task.
          build:js      - Build js from sources (Webpack module bundler).
          build:css     - Build css from sources (Sass files in scss format).
          html          - Copy html files to destination
        Clean:
          clean         - Remove all files (assets, reports, docs) from destination.
          clean:js      - Remove only javascript asset files.
          clean:css     - Remove only css asset files.

        Watch:
          watch         - Watch source files for changes and rebuild assets, rerun tasks.
          watch:asset   - As above, but only assets.
          watch:test    - As above, but only tests.

        Develop:
          devel         - Developer mode - build assets, run webserver, run watch.
          devel:asset   - As above, but only assets.
          devel:test    - As above, but only tests.
          webserver     - Run webserver only.

        Data:
          doc           - Generate documentation in destination (esdoc).
          coverage      - Generate code coverage report (istanbul).
          test          - Run tests and generate report (mocha + chai).
          todo          - Generate todos list report.
        `
    );
});

/**
 * Build js task
 * Create js bundle from entry js
 * Used Webpack module builder
 * Webpack: https://webpack.github.io/
 */
gulp.task('build:js', ['clean:js'], () => {
    const options = tasks.js.webpack.options;

    gulp.src(options.entry)
        .pipe($.webpack(options))
        .pipe(gulp.dest(dir.build));
});

/**
 * Build css task
 * Sass compatible source files (in scss format)
 */
gulp.task('build:css', ['clean:css'], () => {
    const {source, destination, concatenate} = tasks.css;

    return gulp.src(source)
        .pipe($.sass.sync().on('error', $.sass.logError))
        .pipe($.concat(concatenate))
        .pipe($.cssnano())
        .pipe(gulp.dest(destination));
});

/**
 * Build task
 * Build javascript and css assets
 */
gulp.task('build', ['build:js', 'build:css', 'html'], () => {});

/**
 * Code coverage test
 * Checks code coverage with Istanbul library
 * Warning: for es6 using: gulp-babel-istanbul, not gulp-istanbul
 * Istanbul: https://gotwarlost.github.io/istanbul/
 */
gulp.task('coverage', () => {
    const {source, tempDir} = tasks.mocha;

    return gulp.src([source])
        .pipe($.babelIstanbul())
        .pipe($.babelIstanbul.hookRequire())
        .pipe(gulp.dest(tempDir));
});

/**
 * Test task
 * Checks js tests with Mocha and Chai libraries
 * Need explicitly transpile to es6 to istanbul work
 * Additionally used Mochawesome report
 * Mocha: https://mochajs.org/
 * Chai: http://chaijs.com/
 * Mochawesome: http://adamgruber.github.io/mochawesome/
 */
gulp.task('test', ['coverage'], () => {
    const {options, sourceTest, reportDir} = tasks.mocha,
        env = $.env.set({MOCHAWESOME_REPORTDIR: reportDir});

    return gulp.src([sourceTest])
        .pipe(env)
        .pipe($.babel())
        .pipe($.mocha(options))
        .pipe(env.reset)
        .pipe($.babelIstanbul.writeReports(tasks.istanbul.options))
        .pipe($.babelIstanbul.enforceThresholds({ thresholds: { global: 90 } }));
});

/**
 * Documentation task
 * Generate js documentation with ESDoc
 * ESDoc: https://esdoc.org/
 */
gulp.task('doc', () => {
    const {source, options} = tasks.esdoc;
    del(options.destination);

    return gulp.src([source])
        .pipe($.esdoc(options));
});

/**
 * Todo task
 * Generate Todos list from source files
 */
gulp.task('todo', () => {
    const {source, destination} = tasks.todo;

    gulp.src(source)
        .pipe($.todo())
        .pipe(gulp.dest(destination));
});

/**
 * Clean css task
 * Remove all css files from destination
 */
gulp.task('clean:css', () => {
    return del(tasks.clean.css);
});

/**
 * Clean javascript task
 * Remove all javascript files from destination
 */
gulp.task('clean:js', () => {
    return del(tasks.clean.js);
});

/**
 * Clean all task
 * All files in destination are removed
 */
gulp.task('clean', () => {
    return del(tasks.clean.all);
});

/**
 * Watch assets task
 * Watch source files and rebuild assets
 */
gulp.task('watch:asset', () => {
    const logger = ({path}) => {
        $.util.log(`changed: ${path}`);
        gulp.src(path)
            .pipe($.wait(1000))
            .pipe($.connect.reload());
    };

    gulp.watch(tasks.css.source, ['build:css']).on('change', logger);
    gulp.watch(tasks.js.source, ['build:js']).on('change', logger);
    gulp.watch(`${dir.html}/*.html`, ['html']).on('change', logger);
});

/**
 * Watch test task
 * Watch all tests and rerun tests, rebuild reports
 */
gulp.task('watch:test', () => {
    const logger = ({path}) => {
        $.util.log(`changed: ${path}`);
        gulp.src(path)
            .pipe($.wait(1000))
            .pipe($.connect.reload());
    };
    gulp.watch(tasks.mocha.sourceTest, ['test']).on('change', logger);
});

/**
 * Watch task
 * Watch all source files and tests
 */
gulp.task('watch', ['watch:asset', 'watch:test'], () => {});

/**
 * Webserver task
 * Run webserver with life reload capabilities
 */
gulp.task('webserver', () => {
    const {host, port} = tasks.webserver,
        uri = `http://${host}:${port}`;

    $.connect.server(tasks.webserver);
    gulp.src(__filename).pipe($.open({uri}));
});

/**
 * Devel task
 * Build, watch assets, tests and run webserver
 */
gulp.task('devel', ['build', 'webserver', 'watch:asset'], () => {});

/**
 * Devel test task
 * Watch tests and run webserver
 */
gulp.task('devel:test', ['webserver', 'watch:test'], () => {});

/**
 * Devel asset task
 * Watch assets and run webserver
 */
gulp.task('devel:asset', ['build', 'webserver', 'watch'], () => {});

/**
 * Html task
 * Copy html files from source to destination
 */
gulp.task('html', function () {
    return gulp
        .src(`${dir.html}/*.html`)
        .pipe(gulp.dest(dir.build));
});
