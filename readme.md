ACL Charts
==========

Prepare of develop environment
------------------------------

Develop process is mainly done via **Gulp** tool. To get Gulp, install **Node.js** at first.

### 1. Node.js installation

**Node.js v6.0.0** or newer is strictly **required**, beacuse of some ES6 features used in **gulpfile.js**. Go to page: https://nodejs.org/en/download/current/ and get proper verion for your operating system. During installation, remember to check **NPM Package Manager**. After all, verify what version is installed:
> node --version
### 2. Gulp installation
Gulp must be installed always globally and locally for every project. At first, install it globally:
> npm i -g gulp

If you run **npm** under linux, remember to install Gulp globally as root, like via **sudo**.
Local installation of Gulp is not needed explicitly, because next step will do this for you.
### 3. Dependencies installation
Project contains npm **package.json** file that describes project basic information and library dependencies. All you need to do is just simply:
>npm install

By command above, npm will check package.json and install all dependencies: libraries, tools etc. It can take some time, so be patient. All sugar will appear in default **node_modules** directory, that is excluded from project via **.gitignore** file.

***And that's it! have fun!***

Run develop environment
-----------------------
Okey, you have all that you need to develop and check proccess. All tasks that is available for project are given by **Gulp** tool. Run it:
>gulp

You should see of all available commands.

Various technical informations
------------------------------
###1. Build
For building javascript assets, **Webpack** is used. For **Sass** files, normal streams with standard gulp files are implemented.

###2. Tests
For tests, **Mocha** library is used with **Chai** assertion library. Code coverage is checked via **Istanbul** library.

###3. Documentation
Documentation is generated with use of **ESDoc** library.

###4. Ecmascript 6
Code is transpiled via **Babel** with ES2015 preset.





