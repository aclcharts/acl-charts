import jsdom from 'jsdom-global';

export default (nodeName, id) => {
    if (typeof document === 'undefined') {
        jsdom();
    }

    const element = document.createElement(nodeName);
    const body =    document.querySelector('body');
    element.id = id;
    body.appendChild(element);
}
