import Chart from '../../js/acl/Chart';

export default (type, selector) => {
    const types = Chart.getTypes();
    const contexts = Chart.getContexts();
    const settings = {
        target: selector,
        type: types[type],
        context: contexts.SVG,
        updateOnWindowResize: true
    };
    let data;

    if (type === 'Pie' || type === 'Donut') {
        data = [{
            label: "Test 1",
            value: 850
        }, {
            label: "Test 2",
            value: 400
        }, {
            label: "Test 3",
            value: 600
        }, {
            label: "Test 4",
            value: 500
        }]
    } else {
        data = [{
            label: "Test 1",
            value: [850, 720, 965, 653, 385]
        }, {
            label: "Test 2",
            value: [400, 350, 300, 542, 589]
        }, {
            label: "Test 3",
            value: [600, 650, 550, 123, 721]
        }, {
            label: "Test 4",
            value: [-500, -20, 150, 389, 891]
        }, {
            label: "Test 5",
            value: [-330, -120, -58, 179, -238]
        }]
    }

    return {
        settings: settings,
        data: data
    };
}
