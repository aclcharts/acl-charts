import * as chai from 'chai';
import ChartManager from '../../js/acl/ChartManager';
import DataManager from '../../js/acl/data/DataManager';
import SettingsManager from '../../js/acl/settings/SettingsManager';
import DomManager from '../../js/acl/dom/DomManager';
import DrawingManager from '../../js/acl/graphic/draw/DrawingManager';
import PlotterManager from '../../js/acl/plot/PlotterManager';
import TestConfig from '../testData/testConfig.testData';
import mockElement from '../helpers/mockElement.helper';

describe('ChartManager', () => {
    mockElement('div', 'donut3');
    const chartManager = new ChartManager(TestConfig('Donut', '#donut3'));

    it('getDrawingManager should return DrawingManager', () => {
        chai.expect(chartManager.getDrawingManager()).to.be.an.instanceof(DrawingManager);
        chai.expect(chartManager.drawingManager).to.be.an.instanceof(DrawingManager);
    });

    it('getDomManager should return DomManager', () => {
        chai.expect(chartManager.getDomManager()).to.be.an.instanceof(DomManager);
        chai.expect(chartManager.domManager).to.be.an.instanceof(DomManager);
    });

    it('getSettingsManager should return SettingsManager', () => {
        chai.expect(chartManager.getSettingsManager()).to.be.an.instanceof(SettingsManager);
        chai.expect(chartManager.settingsManager).to.be.an.instanceof(SettingsManager);
    });

    it('getDataManager should return DataManager', () => {
        chai.expect(chartManager.getDataManager()).to.be.an.instanceof(DataManager);
        chai.expect(chartManager.dataManager).to.be.an.instanceof(DataManager);
    });

    it('getPlotterManager should return PlotterManager', () => {
        chai.expect(chartManager.getPlotterManager()).to.be.an.instanceof(PlotterManager);
        chai.expect(chartManager.plotterManager).to.be.an.instanceof(PlotterManager);
    });

 });
