import * as chai from 'chai';
import Chart from '../../js/acl/Chart';
import * as ContextEnum from '../../js/acl/graphic/draw/contexts/ContextEnum';
import TestConfig from '../testData/testConfig.testData';
import mockElement from '../helpers/mockElement.helper';

describe('Chart', () => {
    mockElement('div', 'donut');
    mockElement('div', 'pie');
    mockElement('div', 'line');
    mockElement('div', 'bar');
    const donutChart = new Chart(TestConfig('Donut', '#donut'));
    const pieChart   = new Chart(TestConfig('Pie', '#pie'));
    const lineChart  = new Chart(TestConfig('Line', '#line'));
    const barChart   = new Chart(TestConfig('Bar', '#bar'));

    it('should be a class and proper instantiate with different types', () => {
        chai.expect(Chart).to.be.an('function');

        chai.expect(donutChart).to.be.an.instanceof(Chart);
        chai.expect(pieChart).to.be.an.instanceof(Chart);
        chai.expect(lineChart).to.be.an.instanceof(Chart);
        chai.expect(barChart).to.be.an.instanceof(Chart);
    });

    it('should static getCharts', () => {
        const charts = Chart.getCharts();

        charts.forEach((chartInstance) => {
            chai.expect(chartInstance).to.be.an.instanceof(Chart);
        });
    });

    it('should static getContexts', () => {
        chai.expect(Object.keys(Chart.getContexts()))
            .to.have.members(Object.keys(ContextEnum));
    });

});
