import * as chai from 'chai';
import Chart from '../../js/acl/Chart';
import SettingsManager from '../../js/acl/settings/SettingsManager';
import TestConfig from '../testData/testConfig.testData';
import mockElement from '../helpers/mockElement.helper';

describe('ChartFacade', () => {
    mockElement('div', 'donut2');
    const config   = TestConfig('Donut', '#donut2');
    const chart    = new Chart(config);

    it('should methods should be accessed via chart class', () => {
        const keys = ['getSettings', 'settings', 'getData', 'data'];

        keys.forEach((key) => {
            chai.expect(chart[key]).to.exist;
        });
    });

    it('should getSettings return correct settings given in constructor', () => {
        const originalSettings = config.settings;
        const chartSettings = chart.getSettings();

        for (let setting in originalSettings) {
            chai.expect(originalSettings[setting]).to.equal(chartSettings[setting]);
        }
    });

    it('should getSettings return correct default settings not given in constructor', () => {
        const allDefaultSettings = SettingsManager.getDefault();
        const originalSettings = config.settings;
        const chartSettings = chart.getSettings();

        for (let setting in allDefaultSettings) {
            // check if given by user
            if (typeof originalSettings[setting] === 'undefined') {
                // if not given by user check if is deep equal to default setting
                chai.expect(chartSettings[setting]).to.eql(allDefaultSettings[setting]);
            }
        }
    });

    it('should getData be deep equal with original data', () => {
        const originalData = config.data;
        const chartData    = chart.getData();

        chai.expect(originalData).to.eql(chartData);
    });

    it('should getSettings via property be strict equal with method', () => {
        chai.expect(chart.getSettings()).to.equal(chart.settings);
    });

    it('should getData via property be strict equal with method', () => {
        chai.expect(chart.getData()).to.equal(chart.data);
    });

});
