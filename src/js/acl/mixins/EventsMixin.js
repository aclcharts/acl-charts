import EventsHandler from '../events/EventsHandler';
import EventNotDefinedError from '../errors/EventNotDefinedError';
import EventsAlreadyDefinedError from '../errors/EventsAlreadyDefinedError';

/**
 * EventsHandlers container
 * @type {WeakMap<object,EventsHandler>}
 * @private
 */
const _handlers = new WeakMap();

/**
 * Events types container
 * @type {WeakMap<object,{}>}
 * @private
 */
const _events = new WeakMap();

/**
 * Events mixin
 *
 * Extends subclass with events functionality
 * provided by EventsHandler class.
 *
 * Initialization by constructor isn't supported
 * because of no support for property accessors in es6.
 * Instead this, initEvents method must be called once,
 * by its owner and eventsHandler will be returned.
 *
 * @param {object} baseClass
 */
export default (baseClass) => class extends baseClass {
    /**
     * Init Events
     * Must be called explicitly with events as argument
     * @param {{}} events
     * @returns {EventsHandler}
     */
    initEvents(events) {
        if (_handlers.has(this)) {
            throw new EventsAlreadyDefinedError();
        }
        // Create events handler
        _handlers.set(this, new EventsHandler());
        _events.set(this, events);

        // Define events
        Reflect.ownKeys(events).forEach((key) => {
            _handlers.get(this).define(events[key]);
        });

        return _handlers.get(this);
    }

    /**
     * Get events definitions
     * @returns {{}}
     */
    getEvents() {
        return _events.get(this);
    }

    /**
     * Get events definitions by property
     * @returns {{}}
     */
    get events() {
        return this.getEvents();
    }

    /**
     * Register callback to specific event.
     * Event must be defined already.
     * @param event
     * @param callback
     */
    on(event, callback) {
        const handler = _handlers.get(this);

        if (handler.isDefined(event)) {
            handler.add(event, callback);
        } else {
            throw new EventNotDefinedError(event);
        }
    }
}
