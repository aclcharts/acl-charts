import BaseClass from '../../BaseClass';
import ChartManager from '../../ChartManager';
import * as ContextsEnum from './contexts/ContextEnum';
import CanvasContext from './contexts/CanvasContext';
import SVGContext from './contexts/SVGContext';
import WebGLContext from './contexts/WebGLContext';
import NotInstanceOfError from '../../errors/NotInstanceOfError';
import UnrecognizedDrawContextError from '../../errors/UnrecognizedDrawContextError';

/**
 * ChartManagers private field
 * @type {WeakMap<object,ChartManager>}
 * @private
 */
const _chartManagers = new WeakMap();

/**
 * Contexts private field
 * Stores Objects based on AbstractContext
 * @type {WeakMap<object,AbstractContext>}
 * @private
 */
const _contexts = new WeakMap();

/**
 * Drawing Contexts enumeration
 * @type {{}}
 */
export const contexts = {
    [ContextsEnum.Canvas]: CanvasContext,
    [ContextsEnum.SVG]: SVGContext,
    [ContextsEnum.WebGL]: WebGLContext
};

/**
 * DrawingManager class
 * Responsible for drawing processes
 */
export default class DrawingManager extends BaseClass {
    /**
     * Create new instance
     * @param {object} chartManager
     */
    constructor(chartManager) {
        super();

        if (chartManager instanceof ChartManager === false) {
            throw new NotInstanceOfError(ChartManager);
        }

        _chartManagers.set(this, chartManager);
        _contexts.set(this, this.createContext().init(chartManager.domManager.element).clear());
    }

    /**
     * Create Context
     * Context is of type regardless to settings.context.
     * This setting must have one of values from ContextEnum.
     * @returns {object} Draw Context of interface from AbstractContext
     */
    createContext() {
        const {context} = _chartManagers.get(this).settingsManager.settings;

        if (Reflect.has(contexts, context)) {
            return new contexts[context](this);
        }

        throw new UnrecognizedDrawContextError(context);
    }

    /**
     * Get context
     * @returns {ContextInterface}
     */
    getContext() {
        return _contexts.get(this);
    }

    /**
     * Get context via property
     * @returns {ContextInterface}
     */
    get context() {
        return this.getContext();
    }
}
