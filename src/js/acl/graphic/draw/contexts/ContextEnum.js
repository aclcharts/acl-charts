/**
 * Canvas context
 * @type {Symbol}
 */
export const Canvas = Symbol('Canvas');

/**
 * SVG context
 * @type {Symbol}
 */
export const SVG = Symbol('SVG');

/**
 * WebGL context
 * @type {Symbol}
 */
export const WebGL = Symbol('WebGL');
