import AbstractContext from './AbstractContext';

const namespace = 'http://www.w3.org/2000/svg';

/**
 * Container for Element
 * @type {WeakMap<object,Element>}
 * @private
 */
const _elements = new WeakMap();

/**
 * Container for SVG elements
 * @type {WeakMap<object,Element>}
 * @private
 */
const _svg = new WeakMap();

/**
 * Container for actual fill color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _fillColors = new WeakMap();

/**
 * Container for actual stroke color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _strokeColors = new WeakMap();

/**
 * Container for lineWidth
 * @type {WeakMap<object,number>}
 * @private
 */
const _lineWidth = new WeakMap();

/**
 * Container for actual clear color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _clearColors = new WeakMap();

/**
 * Container for font family
 * @type {WeakMap,<object,string>}
 * @private
 */
const _fontFamily = new WeakMap();

/**
 * Container for font size
 * @type {WeakMap<object,number>}
 * @private
 */
const _fontSize = new WeakMap();

/**
 * SVGContext class
 * @extends AbstractContext
 */
export default class SVGContext extends AbstractContext {
    /**
     * Create instance
     * @param drawingManager
     */
    constructor(drawingManager) {
        super(drawingManager);
    }

    /**
     * Initialize Context
     * @param {Element} element
     * @returns {SVGContext}
     *
     */
    init(element) {
        const svg = document.createElementNS(namespace, 'svg');
        element.appendChild(svg);

        // Store element and svg container
        _elements.set(this, element);
        _svg.set(this, svg);

        svg.setAttribute('width', element.clientWidth);
        svg.setAttribute('height', element.clientHeight);

        super.init();

        return this;
    }

    /**
     * set size of context
     * @param {number} width
     * @param {number} height
     * @returns {SVGContext}
     */
    setSize(width, height) {
        const svg = _svg.get(this);
        svg.setAttribute('width', width);
        svg.setAttribute('height', height);

        return this;
    }

    /**
     * Get size of context
     * @returns {{width: number, height: number}}
     */
    getSize() {
        const element = _elements.get(this);

        return {
            width: element.clientWidth,
            height: element.clientHeight
        };
    }

    /**
     * Clear context
     * @returns {SVGContext}
     */
    clear() {
        const svg = _svg.get(this),
            background = document.createElementNS(namespace, 'rect');

        let cc = _clearColors.get(this);

        // Remove children
        for (let element of [...svg.querySelectorAll('*')]) {
            element.remove();
        }

        svg.appendChild(background);
        background.setAttributeNS(null,'x', 0);
        background.setAttributeNS(null,'y', 0);
        background.setAttributeNS(null,'width', svg.getAttributeNS(null, 'width'));
        background.setAttributeNS(null,'height', svg.getAttributeNS(null, 'height'));
        background.setAttributeNS(null,'fill', `rgba(${cc.r},${cc.g},${cc.b},${cc.a})`);
        background.setAttributeNS(null,'stroke','none');

        return this;
    }

    /**
     * Set fill color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {SVGContext}
     */
    setFillColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _fillColors.set(this, r);
        } else {
            _fillColors.set(this, {r, g, b, a});
        }

        return this;
    }

    /**
     * Set stroke color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {SVGContext}
     */
    setStrokeColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _strokeColors.set(this, r);
        } else {
            _strokeColors.set(this, {r, g, b, a});
        }

        return this;
    }

    /**
     * Set clear color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {SVGContext}
     */
    setClearColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _clearColors.set(this, r);
        } else {
            _clearColors.set(this, {r, g, b, a});
        }

        return this;
    }

    /**
     * Set line width
     * @param {number} width
     * @returns {SVGContext}
     */
    setLineWidth(width) {
        _lineWidth.set(this, width);

        return this;
    }

    /**
     * Set font family
     * @param {string} font
     * @returns {SVGContext}
     */
    setFontFamily(font) {
        _fontFamily.set(this, font);

        return this;
    }

    /**
     * Set font size
     * @param {number} size
     * @returns {SVGContext}
     */
    setFontSize(size) {
        _fontSize.set(this, size);

        return this;
    }

    /**
     * Get font size
     * @returns {number}
     */
    getFontSize() {
        return _fontSize.get(this);
    }

    /**
     * Draw text
     * @param {number} x
     * @param {number} y
     * @param {string} text
     * @returns {SVGContext}
     */
    drawText(x, y, text) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            textP = document.createElementNS(namespace, 'text');

        let dy;

        switch (this.getTextBaseline()) {
            case 'middle':
                dy = '0.6em';
                break;

            case 'bottom':
                dy = '-0.6em';
                break;

            default:
                dy = '1.2em';
        }

        svg.appendChild(textP);
        textP.setAttributeNS(null, 'x', x);
        textP.setAttributeNS(null, 'y', y);
        textP.setAttributeNS(null, 'dy', dy);
        textP.setAttributeNS(null, 'fill', `rgba(${fc.r},${fc.g},${fc.b},${fc.a})`);
        textP.setAttributeNS(null, 'font-family', _fontFamily.get(this));
        textP.setAttributeNS(null, 'font-size', _fontSize.get(this));
        textP.setAttributeNS(null, 'text-anchor', this.getTextAlign());
        textP.appendChild(document.createTextNode(text));

        return this;
    }

    /**
     * Draw rectangle
     * @param {number} x
     * @param {number} y
     * @param {number} w - width
     * @param {number} h - height
     * @returns {SVGContext}
     */
    drawRectangle(x, y, w, h) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            sc = _strokeColors.get(this),
            rect = document.createElementNS(namespace, 'rect');

        // SVG doesn't accept negative values on width and height
        // so make some corrections if required
        if (w < 0) {
            x += w;
            w *= -1;
        }

        if (h < 0) {
            y += h;
            h *= -1;
        }

        svg.appendChild(rect);
        rect.setAttributeNS(null,'x', x);
        rect.setAttributeNS(null,'y', y);
        rect.setAttributeNS(null,'width', w);
        rect.setAttributeNS(null,'height', h);
        rect.setAttributeNS(null,'fill', this.getFill() ? `rgba(${fc.r},${fc.g},${fc.b},${fc.a})` : 'none');
        rect.setAttributeNS(null,'stroke', this.getStroke() ? `rgba(${sc.r},${sc.g},${sc.b},${sc.a})` : 'none');
        rect.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw polygon
     * @override
     * @param {[]} points coordinates
     * @returns {SVGContext}
     */
    drawPoly(points) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            sc = _strokeColors.get(this),
            path = document.createElementNS(namespace, 'path');

        let formatted = `M ${points[0]} ${points[1]} `;

        for (let i = 0; i < points.length; i += 2) {
            formatted += `L ${points[i]} ${points[i+1]} `;
        }

        formatted += 'Z';

        svg.appendChild(path);
        path.setAttributeNS(null,'d', formatted);
        path.setAttributeNS(null,'fill', this.getFill() ? `rgba(${fc.r},${fc.g},${fc.b},${fc.a})` : 'none');
        path.setAttributeNS(null,'stroke', this.getStroke() ? `rgba(${sc.r},${sc.g},${sc.b},${sc.a})` : 'none');
        path.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw line
     * @param {number} x
     * @param {number} y
     * @param {number} x2
     * @param {number} y2
     * @returns {SVGContext}
     */
    drawLine(x, y, x2, y2) {
        const svg = _svg.get(this),
            sc = _strokeColors.get(this),
            line = document.createElementNS(namespace, 'line');

        svg.appendChild(line);
        line.setAttributeNS(null,'x1', x);
        line.setAttributeNS(null,'y1', y);
        line.setAttributeNS(null,'x2', x2);
        line.setAttributeNS(null,'y2', y2);
        line.setAttributeNS(null,'stroke', `rgba(${sc.r},${sc.g},${sc.b},${sc.a})`);
        line.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw multi line
     * @param {[]} points
     * @returns {SVGContext}
     */
    drawMultiLine(points) {
        const svg = _svg.get(this),
            sc = _strokeColors.get(this),
            polyline = document.createElementNS(namespace, 'polyline');

        let formatted = "";

        for (let i = 0; i < points.length; i += 2) {
            formatted += `${points[i]},${points[i+1]} `;
        }

        svg.appendChild(polyline);
        polyline.setAttributeNS(null,'points', formatted);
        polyline.setAttributeNS(null,'fill', 'none');
        polyline.setAttributeNS(null,'stroke', `rgba(${sc.r},${sc.g},${sc.b},${sc.a})`);
        polyline.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw circle
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     */
    drawCircle(x, y, radius) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            sc = _strokeColors.get(this),
            circle = document.createElementNS(namespace, 'circle');

        svg.appendChild(circle);
        circle.setAttributeNS(null,'cx', x);
        circle.setAttributeNS(null,'cy', y);
        circle.setAttributeNS(null,'r', radius);
        circle.setAttributeNS(null,'fill', this.getFill() ? `rgba(${fc.r},${fc.g},${fc.b},${fc.a})` : 'none');
        circle.setAttributeNS(null,'stroke', this.getStroke() ? `rgba(${sc.r},${sc.g},${sc.b},${sc.a})` : 'none');
        circle.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw pie
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawPie(x, y, radius, startAngle, endAngle) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            sc = _strokeColors.get(this),
            path = document.createElementNS(namespace, 'path');

        let formatted = `M ${x} ${y} `;

        const offset = Math.PI - Math.PI / 2;
        let sx = x + Math.sin(offset - startAngle) * radius;
        let sy = y + Math.cos(offset - startAngle) * radius;

        let ex = x + Math.sin(offset - endAngle) * radius;
        let ey = y + Math.cos(offset - endAngle) * radius;

        formatted += `L ${sx} ${sy} `;

        if (Math.abs(endAngle - startAngle) > Math.PI) {
            formatted += `A ${radius} ${radius} 0 1 1 ${ex} ${ey} z`;
        } else {
            formatted += `A ${radius} ${radius} 0 0 1 ${ex} ${ey} z`;
        }

        svg.appendChild(path);
        path.setAttributeNS(null,'d', formatted);
        path.setAttributeNS(null,'fill', this.getFill() ? `rgba(${fc.r},${fc.g},${fc.b},${fc.a})` : 'none');
        path.setAttributeNS(null,'stroke', this.getStroke() ? `rgba(${sc.r},${sc.g},${sc.b},${sc.a})` : 'none');
        path.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }

    /**
     * Draw arc
     * @param {number} x
     * @param {number} y
     * @param {number} radius1
     * @param {number} radius2
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawArc(x, y, radius1, radius2, startAngle, endAngle) {
        const svg = _svg.get(this),
            fc = _fillColors.get(this),
            sc = _strokeColors.get(this),
            path = document.createElementNS(namespace, 'path');

        let formatted = `M ${x} ${y} `;

        const offset = Math.PI - Math.PI / 2;
        let sx1 = x + Math.sin(offset - startAngle) * radius1;
        let sy1 = y + Math.cos(offset - startAngle) * radius1;
        let sx2 = x + Math.sin(offset - startAngle) * radius2;
        let sy2 = y + Math.cos(offset - startAngle) * radius2;

        let ex1 = x + Math.sin(offset - endAngle) * radius1;
        let ey1 = y + Math.cos(offset - endAngle) * radius1;
        let ex2 = x + Math.sin(offset - endAngle) * radius2;
        let ey2 = y + Math.cos(offset - endAngle) * radius2;

        let curveOut = (Math.abs(endAngle - startAngle) > Math.PI) ? "0 1 1" : "0 0 1";
        let curveIn = (Math.abs(endAngle - startAngle) > Math.PI) ? "1 1 0" : "1 0 0";

        formatted += `M ${sx2} ${sy2} `;
        formatted += `L ${sx1} ${sy1} `;
        formatted += `A ${radius1} ${radius1} ${curveOut} ${ex1} ${ey1} `;
        formatted += `L ${ex2} ${ey2} `;
        formatted += `A ${radius2} ${radius2} ${curveIn} ${sx2} ${sy2} `;

        svg.appendChild(path);
        path.setAttributeNS(null,'d', formatted);
        path.setAttributeNS(null,'fill', this.getFill() ? `rgba(${fc.r},${fc.g},${fc.b},${fc.a})` : 'none');
        path.setAttributeNS(null,'stroke', this.getStroke() ? `rgba(${sc.r},${sc.g},${sc.b},${sc.a})` : 'none');
        path.setAttributeNS(null,'stroke-width', _lineWidth.get(this));

        return this;
    }
}
