import NotImplementedError from '../../../errors/NotImplementedError';
import InstantiateInterfaceError from '../../../errors/InstantiateInterfaceError';

/**
 * ContextInterface
 * @interface
 */
export default class ContextInterface {
    constructor() {
        if (this.constructor === ContextInterface) {
            throw new InstantiateInterfaceError(ContextInterface);
        }
    }

    /**
     * Initialize context
     * @abstract
     * @param {Element} element
     */
    init(element) {
        throw new NotImplementedError();
    }

    /**
     * Set size of viewport
     * @abstract
     * @param {number} width
     * @param {number} height
     */
    setSize(width, height) {
        throw new NotImplementedError();
    }

    /**
     * Get size of context
     */
    getSize() {
        throw new NotImplementedError();
    }

    /**
     * Clear context
     * @abstract
     */
    clear() {
        throw new NotImplementedError();
    }

    /**
     * Set fill color
     * @abstract
     * @param {number} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     */
    setFillColor(r, g, b, a = 1) {
        throw new NotImplementedError();
    }

    /**
     * Set stroke color
     * @abstract
     * @param {number} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     */
    setStrokeColor(r, g, b, a = 1) {
        throw new NotImplementedError();
    }

    /**
     * Set clear color
     * @abstract
     * @param {number} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     */
    setClearColor(r, g, b, a = 1) {
        throw new NotImplementedError();
    }

    /**
     * Set line width
     * @abstract
     * @param {number} width
     */
    setLineWidth(width) {
        throw new NotImplementedError();
    }

    /**
     * Set font family
     * @abstract
     * @param {string} font
     */
    setFontFamily(font) {
        throw new NotImplementedError();
    }

    /**
     * Set font size
     * @param {number} size
     */
    setFontSize(size) {
        throw new NotImplementedError();
    }

    /**
     * Get font size
     * @returns {number} size
     */
    getFontSize() {
        throw new NotImplementedError();
    }

    /**
     * Set text align
     * @param {string} align
     */
    setTextAlign(align) {
        throw new NotImplementedError();
    }

    /**
     * Get text align
     */
    getTextAlign() {
        throw new NotImplementedError();
    }

    /**
     * Set text baseline
     * @param {string} baseline
     */
    setTextBaseline(baseline) {
        throw new NotImplementedError();
    }

    /**
     * Get text baseline
     */
    getTextBaseline() {
        throw new NotImplementedError();
    }

    /**
     * Draw text
     * @abstract
     * @param {number} x
     * @param {number} y
     * @param {string} text
     */
    drawText(x, y, text) {
        throw new NotImplementedError();
    }

    /**
     * Draw rectangle
     * @abstract
     * @param {number} x
     * @param {number} y
     * @param {number} w - width
     * @param {number} h - height
     */
    drawRectangle(x, y, w, h) {
        throw new NotImplementedError();
    }

    /**
     * Draw polygon
     * @abstract
     * @param {[]} points coordinates
     */
    drawPoly(points) {
        throw new NotImplementedError();
    }

    /**
     * Draw line
     * @abstract
     * @param {number} x
     * @param {number} y
     * @param {number} x2
     * @param {number} y2
     */
    drawLine(x, y, x2, y2) {
        throw new NotImplementedError();
    }

    /**
     * Draw multi line
     * @abstract
     * @param {[]} points
     */
    drawMultiLine(points) {
        throw new NotImplementedError();
    }

    /**
     * Draw circle
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     */
    drawCircle(x, y, radius) {
        throw new NotImplementedError();
    }

    /**
     * Draw pie
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawPie(x, y, radius, startAngle, endAngle) {
        throw new NotImplementedError();
    }

    /**
     * Draw arc
     * @param {number} x
     * @param {number} y
     * @param {number} radius1
     * @param {number} radius2
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawArc(x, y, radius1, radius2, startAngle, endAngle) {
        throw new NotImplementedError();
    }

    /**
     * Set stroke state
     * @param {bool} state
     */
    setStroke(state) {
        throw new NotImplementedError();
    }

    /**
     * Set fill state
     * @param {bool} state
     */
    setFill(state) {
        throw new NotImplementedError();
    }

    /**
     * Get stroke state
     */
    getStroke() {
        throw new NotImplementedError();
    }

    /**
     * Get fill state
     */
    getFill() {
        throw new NotImplementedError();
    }

    // TODO: implement destroy
    // /**
    //  * Destroy context
    //  * @abstract
    //  */
    // destroy() {
    //     throw new NotImplementedError();
    // }
}
