/**
 * Vertices program
 * @type {Symbol}
 */
export const Vertices = Symbol('Vertices');

/**
 * Image program
 * @type {Symbol}
 */
export const Image = Symbol('Image');
