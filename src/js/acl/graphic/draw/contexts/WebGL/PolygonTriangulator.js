// TODO: change to some other triangulator

/**
 * Get area of polygon
 * @param {[]} poly
 * @returns {number}
 */
function area(poly) {
    const n = poly.length - 1;
    let a = 0;
    let p = n - 1;

    for (let q = 0; q < n; q += 2) {
        a = a + poly[p] * poly[q + 1] - poly[q] * poly[p + 1];
        p = q;
    }
    return a * 0.5;
}

/**
 * Check if point P is inside triangle (A,B,C)
 * @param {number} Ax
 * @param {number} Ay
 * @param {number} Bx
 * @param {number} By
 * @param {number} Cx
 * @param {number} Cy
 * @param {number} Px
 * @param {number} Py
 * @returns {boolean}
 */
function insideTriangle(Ax, Ay, Bx, By, Cx, Cy, Px, Py) {
    let ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;

    ax = Cx - Bx;
    ay = Cy - By;
    bpx = Px - Bx;
    bpy = Py - By;

    if ((ax * bpy - ay * bpx) < 0) {
        return false;
    }

    bx = Ax - Cx;
    by = Ay - Cy;
    cpx = Px - Cx;
    cpy = Py - Cy;

    if ((bx * cpy - by * cpx) < 0) {
        return false;
    }

    cx = Bx - Ax;
    cy = By - Ay;
    apx = Px - Ax;
    apy = Py - Ay;

    return (cx * apy - cy * apx) >= 0;
}

var EPSILON = 0.000001;


function snip(contour, u, v, w, n, V) {
    let Ax, Ay, Bx, By, Cx, Cy, Px, Py;

    Ax = contour[V[u]];
    Ay = contour[V[u] + 1];

    Bx = contour[V[v]];
    By = contour[V[v] + 1];

    Cx = contour[V[w]];
    Cy = contour[V[w] + 1];

    if (EPSILON > (((Bx - Ax) * (Cy - Ay)) - ((By - Ay) * (Cx - Ax)))) {
        return false;
    }

    for (let p = 0; p < n; p++) {
        if ((p == u) || (p == v) || (p == w)) {
        } else {
            Px = contour[V[p]];
            Py = contour[V[p] + 1];

            if (insideTriangle(Ax, Ay, Bx, By, Cx, Cy, Px, Py)) {
                return false;
            }
        }
    }

    return true
}

/**
 * Triangulate poly
 * @param {[]} poly
 * @returns {[]}
 */
function triangulate(poly) {
    const result = [];

    if (poly.length < 6) {
        return null;
    }

    let nv = Math.floor(poly.length / 2);

    const V = [];

    if (area(poly) >= 0) {
        for (let i = 0; i < nv; i++) {
            V[i] = i * 2;
        }
    } else {
        for (let i = 0; i < nv; i++) {
            V[i] = poly.length - i * 2 - 2;
        }
    }

    let count = nv * 2;
    let v = nv;

    while (nv > 2) {
        if (--count < 0) return null;

        let u = v;
        if (u >= nv) u = 0;
        v = u + 1;
        if (v >= nv) v = 0;
        let w = v + 1;
        if (w >= nv) w = 0;
        if (snip(poly, u, v, w, nv, V)) {
            let a = V[u];
            let b = V[v];
            let c = V[w];
            result.push(poly[a]);
            result.push(poly[a + 1]);
            result.push(poly[b]);
            result.push(poly[b + 1]);
            result.push(poly[c]);
            result.push(poly[c + 1]);
            V.splice(v, 1);
            nv--;
            count = nv * 2;
        }
    }

    return result;
}

/**
 * PolygonTriangulator class
 */
export default class PolygonTriangulator {
    /**
     * Translate polygon to array of triangles
     * @param points
     */
    static triangulate(points) {
        return triangulate(points);
    }
}
