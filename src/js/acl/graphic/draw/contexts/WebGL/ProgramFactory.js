import WebGLProgramNotDefinedError from '../../../../errors/WebGLProgramNotDefinedError';
import ProgramInterface from './programs/ProgramInterface';
import * as Programs from './ProgramEnum';
import ImageProgram from './programs/ImageProgram';
import VerticesProgram from './programs/VerticesProgram';

/**
 * ProgramFactory class
 * Responsible for creating GL Programs
 */
export default class ProgramFactory {
    /**
     * Create program
     * @param {WebGLContext} gl
     * @param {Symbol} program type
     * @return {ProgramInterface}
     */
    create(gl, program) {
        switch (program) {
            case Programs.Vertices:
                return new VerticesProgram(gl);
                break;

            case Programs.Image:
                return new ImageProgram(gl);
                break;

            default:
                throw new WebGLProgramNotDefinedError(program);
        }
    }
}
