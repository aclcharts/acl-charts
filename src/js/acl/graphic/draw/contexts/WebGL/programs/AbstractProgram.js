import ProgramInterface from './ProgramInterface';
import ShaderCompiler from './ShaderCompiler';

/**
 * Container for WebGLContext
 * @type {WeakMap<object,WebGLContext>}
 */
const _gl = new WeakMap();
/**
 * Container for WebGLProgram programs
 * @type {WeakMap<object,WebGLProgram>}
 * @private
 */
const _program = new WeakMap();

/**
 * Container for attribute locations
 * @type {WeakMap<object,Map>}
 * @private
 */
const _attributeLocation = new WeakMap();

/**
 * Container for uniform locations
 * @type {WeakMap<object,Map>}
 * @private
 */
const _uniformLocation = new WeakMap();

/**
 *
 */
export default class AbstractProgram extends ProgramInterface {
    /**
     * Initialize Program
     * @override
     * @param {WebGLContext} gl
     * @param {string} vertex - vertex shader
     * @param {string} fragment - fragment shader
     * @returns {AbstractProgram}
     */
    init(gl, vertex, fragment) {
        if (_program.get(this)) {
            throw new WebGLProgramAlreadyInitializedError();
        }

        const program = gl.createProgram(),
            vertexCompiled = ShaderCompiler.getCompiledVertexShader(gl, vertex),
            fragmentCompiled = ShaderCompiler.getCompiledFragmentShader(gl, fragment);

        _gl.set(this, gl);
        _program.set(this, program);
        _attributeLocation.set(this, new Map());
        _uniformLocation.set(this, new Map());

        gl.attachShader(program, vertexCompiled);
        gl.attachShader(program, fragmentCompiled);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            throw new WebGLError(gl.getProgramInfoLog(program));
        }

        return this;
    }

    /**
     * Get compiled GL program
     * @returns {WebGLProgram}
     */
    getGLProgram() {
        return _program.get(this);
    }

    /**
     * Get compiled GL program via property
     * @returns {WebGLProgram}
     */
    get glProgram() {
        return this.getGLProgram();
    }

    /**
     * Create attribute location
     * @param {string} name
     * @returns {AbstractProgram}
     */
    createAttributeLocation(name) {
        const gl = _gl.get(this),
            program = _program.get(this);

        _attributeLocation.get(this).set(name, gl.getAttribLocation(program, name));

        return this;
    }

    /**
     * Create uniform location
     * @param {string} name of uniform location
     * @returns {AbstractProgram}
     */
    createUniformLocation(name) {
        const gl = _gl.get(this),
            program = _program.get(this);

        _uniformLocation.get(this).set(name, gl.getUniformLocation(program, name));

        return this;
    }

    /**
     * Get attribute location
     * @param {string} name of attribute location
     * @returns {GLint}
     */
    getAttributeLocation(name) {
        return _attributeLocation.get(this).get(name);
    }

    /**
     * Get uniform location
     * @param {string} name of uniform location
     * @returns {WebGLUniformLocation}
     */
    getUniformLocation(name) {
        return _uniformLocation.get(this).get(name);
    }
}
