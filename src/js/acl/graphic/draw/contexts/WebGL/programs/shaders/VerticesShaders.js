
/**
 * Fragment shader responsible for rendering vertices
 * @type {string}
 */
export const fragment = `
    precision mediump float;
    uniform vec4 u_color;

    void main() {
        gl_FragColor = u_color;
    }
`;

/**
 * Vertex shader responsible for rendering vertices with pixel units support
 * @type {string}
 */
export const vertex = `
    precision mediump float;
    attribute vec2 a_position;
    uniform vec2 u_resolution;

    void main() {
        // rescale from pixels to <0,1>
        vec2 rescaled = a_position / u_resolution;

        // Rescale data to 2x and center
        vec2 centered = (rescaled * 2.0) - 1.0;

        // inverse vertically to make 0,0 on top left corner
        gl_Position = vec4(centered * vec2(1, -1), 0, 1);
    }
`;
