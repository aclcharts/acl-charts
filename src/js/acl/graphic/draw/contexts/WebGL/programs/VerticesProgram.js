import AbstractProgram from './AbstractProgram';
import * as Shaders from './shaders/VerticesShaders';

/**
 * VerticesProgram class
 * @extends {AbstractProgram}
 * @implements {ProgramInterface}
 */
export default class VerticesProgram extends AbstractProgram {
    /**
     * Initialize program for given WebGLContext
     * @param {WebGLContext} gl
     * @returns {VerticesProgram}
     */
    init(gl) {
        super.init(gl, Shaders.vertex, Shaders.fragment);
        this.createAttributeLocation('a_position');
        this.createUniformLocation('u_resolution');
        this.createUniformLocation('u_color');

        return this;
    }
}
