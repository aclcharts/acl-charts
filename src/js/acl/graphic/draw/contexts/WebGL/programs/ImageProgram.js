import AbstractProgram from './AbstractProgram';
import * as Shaders from './shaders/ImageShaders';

/**
 * ImageProgram class
 * @extends {AbstractProgram}
 * @implements {ProgramInterface}
 */
export default class ImageProgram extends AbstractProgram {
    /**
     * Initialize program for given WebGLContext
     * @param {WebGLContext} gl
     * @returns {VerticesProgram}
     */
    init(gl) {
        super.init(gl, Shaders.vertex, Shaders.fragment);
        this.createAttributeLocation('a_position');
        this.createUniformLocation('u_resolution');
        this.createAttributeLocation('a_texcoord');

        return this;
    }
}
