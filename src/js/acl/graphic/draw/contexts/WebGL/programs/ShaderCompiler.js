import WebGLError from '../../../../../errors/WebGLProgramShaderError';

/**
 * ShaderCompiler class
 * Provides static methods for compiling program shaders
 */
export default class ShaderCompiler {
    /**
     * Compile fragment shader
     * @param {WebGLRenderingContext} gl
     * @param {string} shader
     * @returns {WebGLShader}
     */
    static getCompiledFragmentShader(gl, shader) {
        return this.getCompiledShader(gl, shader, gl.FRAGMENT_SHADER);
    }

    /**
     * Compile vertex shader
     * @param {WebGLRenderingContext} gl
     * @param {string} shader
     * @returns {WebGLShader}
     */
    static getCompiledVertexShader(gl, shader) {
        return this.getCompiledShader(gl, shader, gl.VERTEX_SHADER);
    }

    /**
     * Compile shader from string format
     * @param {WebGLRenderingContext} gl
     * @param {string} shader
     * @param {*} type
     * @returns {WebGLShader}
     */
    static getCompiledShader(gl, shader, type) {
        const s = gl.createShader(type);
        gl.shaderSource(s, shader);
        gl.compileShader(s);

        // Check if shader was compiled successfully
        if (!gl.getShaderParameter(s, gl.COMPILE_STATUS)) {
            throw new WebGLError(gl.getShaderInfoLog(s));
        }

        return s;
    }
}
