import NotImplementedError from '../../../../../errors/NotImplementedError';
import InstantiateInterfaceError from '../../../../../errors/InstantiateInterfaceError';

/**
 * ProgramInterface
 * @interface
 */
export default class ProgramInterface {
    constructor() {
        if (this.constructor === ProgramInterface) {
            throw new InstantiateInterfaceError(ProgramInterface);
        }
    }

    /**
     * Initialize program for given WebGLContext
     * @abstract
     * @param {WebGLContext} gl
     */
    init(gl) {
        throw new NotImplementedError();
    }

    /**
     * Get WebGLProgram
     * @abstract
     */
    getGLProgram() {
        throw new NotImplementedError();
    }

    /**
     * Create attribute location
     * @abstract
     * @param name
     */
    createAttributeLocation(name) {
        throw new NotImplementedError();
    }

    /**
     * Create uniform location
     * @abstract
     * @param name
     */
    createUniformLocation(name) {
        throw new NotImplementedError();
    }

    /**
     * Get attribute location
     * @param name
     */
    getAttributeLocation(name) {
        throw new NotImplementedError();
    }

    /**
     * Get uniform location
     * @param name
     */
    getUniformLocation(name) {
        throw new NotImplementedError();
    }
}
