/**
 * Fragment shader responsible for rendering image
 * @type {string}
 */
export const fragment = `
    precision mediump float;
    uniform sampler2D u_image;
    varying vec2 v_texcoord;

    void main() {
        gl_FragColor = texture2D(u_image, v_texcoord);
    }
`;

/**
 * Vertex shader responsible for rendering image
 * @type {string}
 */
export const vertex = `
    precision mediump float;
    attribute vec2 a_position;
    attribute vec2 a_texcoord;
    uniform vec2 u_resolution;
    varying vec2 v_texcoord;

    void main() {
        // rescale from pixels to <0,1>
        vec2 rescaled = a_position / u_resolution;
    
        // Rescale data to 2x and center
        vec2 centered = (rescaled * 2.0) - 1.0;
    
        // inverse vertically to make 0,0 on top left corner
        gl_Position = vec4(centered * vec2(1, -1), 0, 1);
            
        v_texcoord = a_texcoord;
    }
`;
