import AbstractContext from './AbstractContext';
import ProgramFactory from './WebGL/ProgramFactory';
import PolygonTriangulator from './WebGL/PolygonTriangulator';
import * as Programs from './WebGL/ProgramEnum';

/**
 * Dom Elements container
 * @type {WeakMap<object,Element>}
 * @private
 */
const _elements = new WeakMap();

/**
 * Canvases container
 * @type {WeakMap,<object,HTMLCanvasElement>}
 * @private
 */
const _canvases = new WeakMap();

/**
 * Drawing Contexts container
 * @type {WeakMap<object,WebGLRenderingContext>}
 * @private
 */
const _contexts = new WeakMap();

/**
 * Container for gl programs
 * @type {WeakMap<object,Map>}
 * @private
 */
const _programs = new WeakMap();

/**
 * Container for buffers
 * @type {WeakMap<object,Map>}
 * @private
 */
const _buffers = new WeakMap();

/**
 * Container for actual fill color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _fillColors = new WeakMap();

/**
 * Container for actual stroke color
 * @type {WeakMap<object,string>}
 * @private
 */
const _strokeColors = new WeakMap();

/**
 * Container for actual clear color
 * @type {WeakMap<object,string>}
 * @private
 */
const _clearColors = new WeakMap();

/**
 * Last programs used
 * @type {WeakMap<object,ProgramInterface>}
 * @private
 */
const _lastProgram = new WeakMap();

/**
 * Last color used (fill or stroke)
 * @type {WeakMap<object,string>}
 * @private
 */
const _lastColor = new WeakMap();

/**
 * Container for text context
 * @type {WeakMap<object,CanvasRenderingContext2D>}
 * @private
 */
const _textContexts = new WeakMap();

/**
 * Container for font family
 * @type {WeakMap,<object,string>}
 * @private
 */
const _fontFamily = new WeakMap();

/**
 * Container for font size
 * @type {WeakMap<object,number>}
 * @private
 */
const _fontSize = new WeakMap();

/**
 * Color type enumeration
 * @type {{}}
 */
const colorType = {
    fill: 0,
    stroke: 1
};

/**
 * WebGLContext class
 * @extends AbstractContext
 */
export default class WebGLContext extends AbstractContext {
    /**
     * Create instance
     * @param drawingManager
     */
    constructor(drawingManager) {
        super(drawingManager);
    }

    /**
     * Initialize Context
     * @param {Element} element
     */
    init(element) {
        const canvas = document.createElement('canvas');

        // Set canvas resolution
        canvas.width = element.clientWidth;
        canvas.height = element.clientHeight;

        element.appendChild(canvas);

        _elements.set(this, element);
        _canvases.set(this, canvas);
        _contexts.set(this, canvas.getContext('webgl'));
        _lastColor.set(this, null);

        // Create off screen Canvas as buffer for text rendering
        const textCanvas = document.createElement('canvas');
        const textContext = textCanvas.getContext('2d');

        textCanvas.width = 512;
        textCanvas.height = 512;

        _textContexts.set(this, textContext);

        // Define WebGL context and make shader programs
        const gl = _contexts.get(this);
        const programFactory = new ProgramFactory();
        const imageProgram = programFactory.create(gl, Programs.Image).init(gl);
        const verticesProgram = programFactory.create(gl, Programs.Vertices).init(gl);

        // Save defined programs and buffers
        _programs.set(this, new Map());
        _programs.get(this).set(Programs.Image, imageProgram);
        _programs.get(this).set(Programs.Vertices, verticesProgram);

        // Create buffers
        _buffers.set(this, new Map());
        _buffers.get(this).set('image', gl.createBuffer());
        _buffers.get(this).set('vertices', gl.createBuffer());

        super.init();

        return this;
    }

    /**
     * Switch program
     * @param {*} programEnum
     * @returns {WebGLContext}
     */
    switchProgram(programEnum) {
        // Check if program is already switched to
        if (_lastProgram.get(this) === programEnum) {
            return;
        }

        const gl = _contexts.get(this),
            program = _programs.get(this).get(programEnum);

        gl.useProgram(program.glProgram);
        _lastProgram.set(this, programEnum);

        switch (programEnum) {
            case Programs.Vertices:
                const canvas = _canvases.get(this),
                    resolution = program.getUniformLocation('u_resolution'),
                    position = program.getUniformLocation('u_position');

                gl.uniform2f(resolution, canvas.width, canvas.height);
                gl.bindBuffer(gl.ARRAY_BUFFER, _buffers.get(this).get('vertices'));
                gl.enableVertexAttribArray(position);
                gl.vertexAttribPointer(position, 2, gl.FLOAT, false, 0, 0);
                break;
        }

        return this;
    }

    /**
     * Set size of context
     * @param {number} width
     * @param {number} height
     * @returns {WebGLContext}
     */
    setSize(width, height) {
        const gl = _contexts.get(this),
            canvas = _canvases.get(this);

        canvas.width = width;
        canvas.height = height;

        const program = _programs.get(this).get(_lastProgram.get(this));
        const resolution = program.getUniformLocation('u_resolution');

        gl.uniform2f(resolution, width, height);
        gl.viewport(0, 0, width, height);

        return this;
    }

    /**
     * Get size of context
     * @returns {{width: number, height: number}}
     */
    getSize() {
        const element = _elements.get(this);

        return {
            width: element.clientWidth,
            height: element.clientHeight
        };
    }

    /**
     * Clear context
     * @returns {WebGLContext}
     */
    clear() {
        this.switchProgram(Programs.Vertices);

        const gl = _contexts.get(this),
            cc = _clearColors.get(this);

        gl.clearDepth(1.0);
        gl.clearColor(cc.r / 255, cc.g / 255, cc.b / 255, cc.a);
        gl.clear(gl.COLOR_BUFFER_BIT);

        return this;
    }

    /**
     * Choose color as current
     * WebGL specific for managing stroke and fill colors
     * @param {number} type
     * @returns {WebGLContext}
     */
    chooseColor(type) {
        const gl = _contexts.get(this),
            last = _lastColor.get(this),
            program = _programs.get(this).get(Programs.Vertices);

        this.switchProgram(Programs.Vertices);

        let c = null;

        switch (type) {
            case last:
                return this;

            case colorType.fill:
                c = _fillColors.get(this);
                break;

            case colorType.stroke:
                c = _strokeColors.get(this);
                break;
        }

        _lastColor.set(this, type);
        gl.uniform4f(program.getUniformLocation('u_color'), c.r, c.g, c.b, c.a);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.BLEND);

        return this;
    }

    /**
     * Set fill color
     * @param {number|object} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {WebGLContext}
     */
    setFillColor(r, g, b, a = 1) {
        let c = null;

        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            c = {
                r: r.r / 255,
                g: r.g / 255,
                b: r.b / 255,
                a: r.a
            };
        } else {
            c = {
                r: r / 255,
                g: g / 255,
                b: b / 255,
                a: a
            };
        }

        _textContexts.get(this).fillStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
        _fillColors.set(this, c);
        _lastColor.set(this, null);

        return this;
    }

    /**
     * Set stroke color
     * @param {number|object} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {WebGLContext}
     */
    setStrokeColor(r, g, b, a = 1) {
        let c = null;

        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            c = {
                r: r.r / 255,
                g: r.g / 255,
                b: r.b / 255,
                a: r.a
            };
        } else {
            c = {
                r: r / 255,
                g: g / 255,
                b: b / 255,
                a: a
            };
        }

        _strokeColors.set(this, c);
        _lastColor.set(this, null);

        return this;
    }

    /**
     * Set clear color
     * @param {number|object} r
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {WebGLContext}
     */
    setClearColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _clearColors.set(this, r);
        } else {
            _clearColors.set(this, {r, g, b, a});
        }

        return this;
    }

    /**
     * Set line width
     * @param {number} width
     * @returns {WebGLContext}
     */
    setLineWidth(width) {
        _contexts.get(this).lineWidth(width);
        return this;
    }

    /**
     * Set font family
     * @param {string} font
     * @returns {CanvasContext}
     */
    setFontFamily(font) {
        _fontFamily.set(this, font);
        _textContexts.get(this).font = `${_fontSize.get(this)}px ${font}`;

        return this;
    }

    /**
     * Set font size
     * @param {number} size
     * @returns {CanvasContext}
     */
    setFontSize(size) {
        _fontSize.set(this, size);
        _textContexts.get(this).font = `${size}px ${_fontFamily.get(this)}`;

        return this;
    }

    /**
     * Get font size
     * @returns {number}
     */
    getFontSize() {
        return _fontSize.get(this);
    }

    /**
     * Draw text
     * @param {number} x
     * @param {number} y
     * @param {string} text
     * @returns {WebGLContext}
     */
    drawText(x, y, text) {
        const gl = _contexts.get(this),
            canvas = _canvases.get(this),
            program = _programs.get(this).get(Programs.Image);

        this.switchProgram(Programs.Image);

        const imageLocation = program.getAttributeLocation('a_texcoord');
        const positionLocation = program.getAttributeLocation('a_position');
        const resolutionLocation = program.getUniformLocation('u_resolution');

        const imageData = new Float32Array([0, 0, 0, 1, 1, 0, 1, 1]);
        const imageBuffer = _buffers.get(this).get('image');

        gl.bindBuffer(gl.ARRAY_BUFFER, imageBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, imageData, gl.STATIC_DRAW);
        gl.enableVertexAttribArray(imageLocation);
        gl.vertexAttribPointer(imageLocation, 2, gl.FLOAT, false, 0, 0);

        // Create a texture.
        const texture = gl.createTexture();
        const image = new Image();
        const textContext = _textContexts.get(this);
        const textCanvas = textContext.canvas;
        const w = textCanvas.width;
        const h = textCanvas.height;
        const fc = _fillColors.get(this);

        textContext.clearRect(0, 0, w, h);

        let align = this.getTextAlign();
        align = align === 'middle' ? 'center' : align;

        textContext.textAlign = align;
        textContext.textBaseline = this.getTextBaseline();
        textContext.fillStyle = `rgba(${fc.r*255},${fc.g*255},${fc.b*255},${fc.a})`;
        textContext.fillText(text, w / 2, h / 2);
        image.src = textCanvas.toDataURL();

        // Set the parameters so we can render any size image.
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        gl.bindBuffer(gl.ARRAY_BUFFER, _buffers.get(this).get('vertices'));
        gl.uniform2f(resolutionLocation, canvas.width, canvas.height);
        gl.enableVertexAttribArray(positionLocation);
        gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);

        const verticesData = new Float32Array([
            x - w / 2, y - h / 2,
            x - w / 2, y - h / 2 + h,
            x + w - w / 2, y - h / 2,
            x + w - w / 2, y - h / 2 + h,
        ]);

        gl.bufferData(gl.ARRAY_BUFFER, verticesData, gl.STATIC_DRAW);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

        return this;
    }

    /**
     * Draw rectangle
     * @override
     * @param {number} x
     * @param {number} y
     * @param {number} w - width
     * @param {number} h - height
     * @returns {WebGLContext}
     */
    drawRectangle(x, y, w, h) {
        if (this.getFill()) {
            this.drawPoly([
                x + w, y + h,
                x + w, y,
                x, y,
                x, y + h,
            ]);
        }

        if (this.getStroke()) {
            //this.chooseColor(colorType.stroke);
            //return this;
            this.drawMultiLine([
                x + w, y + h,
                x + w, y,
                x, y,
                x, y + h,
                x + w, y + h
            ]);
        }

        return this;
    }

    /**
     * Draw stroked rectangle
     * @override
     * @param {number} x
     * @param {number} y
     * @param {number} w - width
     * @param {number} h - height
     * @returns {WebGLContext}
     */
    drawStrokedRectangle(x, y, w, h) {
        return this;
    }

    /**
     * Draw polygon
     * @abstract
     * @param {array} points coordinates
     * @returns {WebGLContext}
     */
    drawPoly(points) {
        if (this.getFill()) {
            this.switchProgram(Programs.Vertices);
            this.chooseColor(colorType.fill);

            const gl = _contexts.get(this);
            const triangulated = PolygonTriangulator.triangulate(points);

            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangulated), gl.STATIC_DRAW);
            gl.drawArrays(gl.TRIANGLES, 0, triangulated.length / 2);
        }

        if (this.getStroke()) {
            this.drawMultiLine(points, true);
        }
        return this;
    }

    /**
     * Draw line
     * @param {number} x
     * @param {number} y
     * @param {number} x2
     * @param {number} y2
     * @returns {WebGLContext}
     */
    drawLine(x, y, x2, y2) {
        this.switchProgram(Programs.Vertices);

        const gl = _contexts.get(this);
        const data = new Float32Array([
            x, y, x2, y2
        ]);

        this.chooseColor(colorType.stroke);

        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        gl.drawArrays(gl.LINE_STRIP, 0, 2);

        return this;
    }

    /**
     * Draw multi line
     * @param {[]} points
     * @param {bool=true} loop - check if line must be closed
     * @returns {WebGLContext}
     */
    drawMultiLine(points, loop = false) {
        this.switchProgram(Programs.Vertices);
        this.chooseColor(colorType.stroke);

        const gl = _contexts.get(this);
        const data = new Float32Array(points);

        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
        gl.drawArrays(loop ? gl.LINE_LOOP : gl.LINE_STRIP, 0, points.length / 2);

        return this;
    }

    /**
     * Draw circle
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     */
    drawCircle(x, y, radius) {
        // TODO: need to implement this
        return this;
    }

    /**
     * Draw pie
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawPie(x, y, radius, startAngle, endAngle) {
        // TODO: need to implement this
        return this;
    }

    /**
     * Draw arc
     * @param {number} x
     * @param {number} y
     * @param {number} radius1
     * @param {number} radius2
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawArc(x, y, radius1, radius2, startAngle, endAngle) {
        // TODO: need to implement this
        return this;
    }
}

