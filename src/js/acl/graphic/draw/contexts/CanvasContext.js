import AbstractContext from './AbstractContext';

/**
 * Dom Elements container
 * @type {WeakMap<object,Element>}
 * @private
 */
const _elements = new WeakMap();

/**
 * Canvases container
 * @type {WeakMap<object,Element>}
 * @private
 */
const _canvases = new WeakMap();

/**
 * Drawing Contexts container
 * @type {WeakMap<object,CanvasRenderingContext2D>}
 * @private
 */
const _contexts = new WeakMap();

/**
 * Container for actual fill color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _fillColors = new WeakMap();

/**
 * Container for actual clear color
 * @type {WeakMap<object,{}>}
 * @private
 */
const _clearColors = new WeakMap();

/**
 * Container for font family
 * @type {WeakMap,<object,string>}
 * @private
 */
const _fontFamily = new WeakMap();

/**
 * Container for font size
 * @type {WeakMap<object,number>}
 * @private
 */
const _fontSize = new WeakMap();

/**
 * CanvasContext class
 * @extends AbstractContext
 */
export default class CanvasContext extends AbstractContext {
    /**
     * Create instance
     * @param drawingManager
     */
    constructor(drawingManager) {
        super(drawingManager);
    }

    /**
     * Initialize Context
     * @param {Element} element
     * @returns {CanvasContext}
     */
    init(element) {
        const canvas = document.createElement('canvas');

        canvas.width = element.clientWidth;
        canvas.height = element.clientHeight;

        element.appendChild(canvas);

        _elements.set(this, element);
        _canvases.set(this, canvas);
        _contexts.set(this, canvas.getContext('2d'));

        // run init form base class
        super.init();

        return this;
    }

    /**
     * Set size of context
     * @param {number} width
     * @param {number} height
     * @returns {CanvasContext}
     */
    setSize(width, height) {
        const canvas = _canvases.get(this);

        canvas.width = width;
        canvas.height = height;

        return this;
    }

    /**
     * Get size of context
     * @returns {{width: number, height: number}}
     */
    getSize() {
        const element = _elements.get(this);

        return {
            width: element.clientWidth,
            height: element.clientHeight
        };
    }

    /**
     * Clear context
     * @returns {CanvasContext}
     */
    clear() {
        const {width, height} = _canvases.get(this),
            context = _contexts.get(this),
            fc = _fillColors.get(this),
            cc = _clearColors.get(this);

        context.fillStyle = `rgba(${cc.r},${cc.g},${cc.b},${cc.a})`;
        context.fillRect(0, 0, width, height);
        context.fillStyle = `rgba(${fc.r},${fc.g},${fc.b},${fc.a})`;

        return this;
    }

    /**
     * Set fill color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {CanvasContext}
     */
    setFillColor(r, g, b, a = 1) {
        let c;

        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _fillColors.set(this, c = r);
        } else {
            _fillColors.set(this, c = {r, g, b, a});
        }

        _contexts.get(this).fillStyle = `rgba(${c.r}, ${c.g}, ${c.b}, ${c.a})`;

        return this;
    }

    /**
     * Set stroke color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {CanvasContext}
     */
    setStrokeColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _contexts.get(this).strokeStyle = `rgba(${r.r}, ${r.g}, ${r.b}, ${r.a})`;
        } else {
            _contexts.get(this).strokeStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
        }

        return this;
    }

    /**
     * Set clear color
     * @param {number|object} r - red color or literal object {r,g,b,a}
     * @param {number} g
     * @param {number} b
     * @param {number} [a=1]
     * @returns {CanvasContext}
     */
    setClearColor(r, g, b, a = 1) {
        if (r.constructor == {}.constructor) {
            r.a = r.a !== undefined ? r.a : 1;
            _clearColors.set(this, r);
        } else {
            _clearColors.set(this, {r, g, b, a});
        }

        return this;
    }

    /**
     * Set line width
     * @param {number} width
     * @returns {CanvasContext}
     */
    setLineWidth(width) {
        _contexts.get(this).lineWidth = width;

        return this;
    }

    /**
     * Set font family
     * @param {string} font
     * @returns {CanvasContext}
     */
    setFontFamily(font) {
        _fontFamily.set(this, font);
        _contexts.get(this).font = `${_fontSize.get(this)}px ${font}`;

        return this;
    }

    /**
     * Set font size
     * @param {number} size
     * @returns {CanvasContext}
     */
    setFontSize(size) {
        _fontSize.set(this, size);
        _contexts.get(this).font = `${size}px ${_fontFamily.get(this)}`;

        return this;
    }

    /**
     * Get font size
     * @returns {number}
     */
    getFontSize() {
        return _fontSize.get(this);
    }

    /**
     * Draw text
     * @param {number} x
     * @param {number} y
     * @param {string} text
     * @returns {CanvasContext}
     */
    drawText(x, y, text) {
        const context = _contexts.get(this);

        let align = this.getTextAlign();
        align = align === 'middle' ? 'center' : align;

        context.textAlign = align;
        context.textBaseline = this.getTextBaseline();
        context.fillText(text, x, y);

        return this;
    }

    /**
     * Draw rectangle
     * @param {number} x
     * @param {number} y
     * @param {number} w - width
     * @param {number} h - height
     * @returns {CanvasContext}
     */
    drawRectangle(x, y, w, h) {
        this.getFill() && _contexts.get(this).fillRect(x, y, w, h);
        this.getStroke() && _contexts.get(this).strokeRect(x, y, w, h);

        return this;
    }

    /**
     * Draw polygon
     * @override
     * @param {array} points coordinates
     * @returns {CanvasContext}
     */
    drawPoly(points) {
        const context = _contexts.get(this);

        context.beginPath();

        for (let i = 0; i < points.length ; i +=2) {
            context.lineTo(points[i], points[i+1]);
        }

        context.closePath();

        this.getFill() && context.fill();
        this.getStroke() && context.stroke();

        return this;
    }

    /**
     * Draw line
     * @param {number} x
     * @param {number} y
     * @param {number} x2
     * @param {number} y2
     * @returns {CanvasContext}
     */
    drawLine(x, y, x2, y2) {
        const context = _contexts.get(this);

        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x2, y2);
        context.stroke();

        return this;
    }

    /**
     * Draw multi line
     * @param {[]} points
     * @returns {CanvasContext}
     */
    drawMultiLine(points) {
        const context = _contexts.get(this);
        context.beginPath();

        for (let i = 0; i < points.length ; i +=2) {
            context.lineTo(points[i], points[i+1]);
        }

        context.stroke();

        return this;
    }

    /**
     * Draw circle
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     */
    drawCircle(x, y, radius) {
        const context = _contexts.get(this);
        context.beginPath();
        context.arc(x, y, radius, 0 , 2 * Math.PI);
        this.getFill() && context.fill();
        this.getStroke() && context.stroke();

        return this;
    }

    /**
     * Draw pie
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawPie(x, y, radius, startAngle, endAngle) {
        const context = _contexts.get(this);
        context.beginPath();
        context.arc(x, y, radius, startAngle, endAngle);
        context.lineTo(x, y);
        context.closePath();

        this.getFill() && context.fill();
        this.getStroke() && context.stroke();

        return this;
    }

    /**
     * Draw arc
     * @param {number} x
     * @param {number} y
     * @param {number} radius1
     * @param {number} radius2
     * @param {number} startAngle
     * @param {number} endAngle
     */
    drawArc(x, y, radius1, radius2, startAngle, endAngle) {
        const context = _contexts.get(this);
        const offset = Math.PI - Math.PI / 2;

        let sx1 = x + Math.sin(offset - startAngle) * radius1;
        let sy1 = y + Math.cos(offset - startAngle) * radius1;
        let sx2 = x + Math.sin(offset - startAngle) * radius2;
        let sy2 = y + Math.cos(offset - startAngle) * radius2;

        context.beginPath();
        context.moveTo(sx2, sy2);
        context.lineTo(sx1, sy1);
        context.arc(x, y, radius1, startAngle, endAngle);
        context.arc(x, y, radius2, endAngle, startAngle, true);
        context.closePath();

        this.getFill() && context.fill();
        this.getStroke() && context.stroke();

        return this;
    }
}
