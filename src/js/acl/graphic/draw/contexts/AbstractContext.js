import ContextInterface from './ContextInterface';
import DrawingManager from '../DrawingManager';
import InstantiateAbstractError from '../../../errors/InstantiateAbstractError';
import NotInstanceOfError from '../../../errors/NotInstanceOfError';
import validateInterface from '../../../helpers/InterfaceValidator';
/**
 * DrawingManagers private field
 * @type {WeakMap<object,DrawingManager>}
 * @private
 */
const _managers = new WeakMap();

/**
 * Fill state
 * @type {WeakMap<object,bool>}
 * @private
 */
const _fill = new WeakMap();

/**
 * Stroke state
 * @type {WeakMap<object,bool>}
 * @private
 */
const _stroke = new WeakMap();

/**
 * Text align
 * @type {WeakMap<object,string>}
 * @private
 */
const _textAlign = new WeakMap();

/**
 * Text baseline
 * @type {WeakMap<object,string>}
 * @private
 */
const _textBaseline = new WeakMap();

/**
 * AbstractContext class
 * @implements ContextInterface
 * Used to provide drawing api for specialized contexts
 */
export default class AbstractContext extends ContextInterface {
    constructor(drawingManager) {
        super();

        if (this.constructor === AbstractContext) {
            throw new InstantiateAbstractError(AbstractContext);
        }

        if (drawingManager instanceof DrawingManager === false) {
            throw new NotInstanceOfError(DrawingManager);
        }

        validateInterface(this, ContextInterface);
        _managers.set(this, drawingManager);
    }

    /**
     * Get DrawingManager
     * Get associated DrawingManager to current instance
     * @returns {DrawingManager}
     */
    getDrawingManager() {
        return _managers.get(this);
    }

    init() {
        this.setFill(true);
        this.setStroke(true);
        this.setFillColor(0, 0, 0, 1);
        this.setClearColor(255, 255, 255, 1);
        this.setStrokeColor(255, 255, 255, 1);
        this.setFontFamily('Arial');
        this.setFontSize(18);
        this.setTextAlign('middle');
        this.setTextBaseline('middle');

        return this;
    }

    /**
     * Set fill state
     * @param {bool} state
     * @returns {AbstractContext}
     */
    setFill(state) {
        _fill.set(this, state);

        return this;
    }

    /**
     * Get fill state
     * @returns {bool}
     */
    getFill() {
        return _fill.get(this);
    }

    /**
     * Set stroke state
     * @param {bool} state
     * @returns {AbstractContext}
     */
    setStroke(state) {
        _stroke.set(this, state);

        return this;
    }

    /**
     * Get stroke state
     * @returns {bool}
     */
    getStroke() {
        return _stroke.get(this);
    }

    /**
     * Set text align
     * @param {string} align
     * @returns {AbstractContext}
     */
    setTextAlign(align) {
        _textAlign.set(this, align);

        return this;
    }

    /**
     * Get text align
     * @returns {string}
     */
    getTextAlign() {
        return _textAlign.get(this);
    }

    /**
     * Set text baseline
     * @param {string} baseline
     * @returns {AbstractContext}
     */
    setTextBaseline(baseline) {
        _textBaseline.set(this, baseline);

        return this;
    }

    /**
     * Get text baseline
     * @returns {string}
     */
    getTextBaseline() {
        return _textBaseline.get(this);
    }
}

