/**
 * EventAlreadyDefinedError
 * This class should be used to throw error of already defined event type
 */
export default class EventAlreadyDefinedError extends Error {
    /**
     * Create new instance
     * @param {Symbol} event
     */
    constructor(event) {
        super(`Event already defined: ${String(event)}`);
    }
}
