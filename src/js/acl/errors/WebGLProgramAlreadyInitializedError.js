/**
 * WebGLProgramAlreadyInitializedError
 * This class should be used when WebGL program is already initialized
 */
export default class WebGLProgramAlreadyInitializedError extends Error {
    /**
     * Create new instance
     */
    constructor(name) {
        super(`WebGL program is already initialized`);
    }
}
