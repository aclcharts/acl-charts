/**
 * NotLiteralObjectError
 * This class should be used to throw error when value is not literal object
 */
export default class NotLiteralObjectError extends Error {
    /**
     * Create new instance
     * @param {*} mixed
     */
    constructor(mixed) {
        super(`Literal object excepted, received: ${String(mixed)}`);
    }
}
