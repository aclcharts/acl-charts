/**
 * ColorNotDefinedError
 * Threw when color with given name is not defined
 */
export default class ColorNotDefinedError extends Error {
    /**
     * Create new instance
     * @param {string} color
     */
    constructor(color) {
        super(`Unknown color: ${color}`);
    }
}
