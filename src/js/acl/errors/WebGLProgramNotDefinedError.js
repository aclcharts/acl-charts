/**
 * WebGLProgramNotDefinedError
 * This class should be used when WebGL program is not defined
 */
export default class WebGLProgramNotDefinedError extends Error {
    /**
     * Create new instance
     * @param {string} name
     */
    constructor(name) {
        super(`WebGL program not defined: ${String(name)}`);
    }
}
