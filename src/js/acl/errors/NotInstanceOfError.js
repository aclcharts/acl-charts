/**
 * NotInstanceOfError
 * This class should be used to signal not instance of given type
 */
export default class NotInstanceOfError extends Error {
    /**
     * Create new instance
     * @param {class} prototype
     */
    constructor(prototype) {
        super(`Object is not instance of ${prototype.name}`);
    }
}
