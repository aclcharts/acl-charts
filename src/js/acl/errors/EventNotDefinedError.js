/**
 * EventNotDefinedError
 * This class should be used to throw error of not defined event type
 */
export default class EventNotDefinedError extends Error {
    /**
     * Create new instance
     * @param {Symbol} event
     */
    constructor(event) {
        super(`Event not defined: ${String(event)}`);
    }
}
