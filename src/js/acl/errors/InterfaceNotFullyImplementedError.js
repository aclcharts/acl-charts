/**
 * InterfaceNotFullyImplementedError
 * This class should be used to throw error when target not fully implements some interface methods
 */
export default class InterfaceNotFullyImplementedError extends Error {
    /**
     * Create new instance
     * @param {string} targetClass
     * @param {string} interfaceClass
     */
    constructor(targetClass, interfaceClass) {
        super(`Interface implementation error: ${targetClass} not implements some methods of ${interfaceClass}`);
    }
}