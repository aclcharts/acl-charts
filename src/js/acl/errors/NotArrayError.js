/**
 * NotArrayError
 * This class should be used to throw error when value is not array
 */
export default class NotArrayError extends Error {
    /**
     * Create new instance
     * @param {*} mixed
     */
    constructor(mixed) {
        super(`Array excepted, received: ${String(mixed)}`);
    }
}
