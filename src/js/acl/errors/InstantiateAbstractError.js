/**
 * InstantiateAbstractError
 * This class should be used to prevent instantiate abstract class
 */
export default class InstantiateAbstractError extends Error {
    /**
     * Create new instance
     * @param {class} prototype
     */
    constructor(prototype) {
        super(`Cannot instantiate abstract class: ${prototype.name}`);
    }
}
