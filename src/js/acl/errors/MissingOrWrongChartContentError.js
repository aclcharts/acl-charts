/**
 * MissingOrWrongChartContentError
 * This class should be used to throw error when chart content is wrong
 */
export default class MissingOrWrongChartContentError extends Error {
    /**
     * Create new instance
     * @param {*} mixed
     */
    constructor(mixed) {
        super(`Correct chart content expected [{x: Number, y:Number}], received: ${String(mixed)}`);
    }
}
