/**
 * InstantiateInterfaceError
 * This class should be used to prevent instantiate interface
 */
export default class InstantiateInterfaceError extends Error {
    /**
     * Create new instance
     * @param {class} prototype
     */
    constructor(prototype) {
        super(`Cannot instantiate interface: ${prototype.name}`);
    }
}
