/**
 * WebGLProgramShaderError
 * This class should be used to throw error when WebGL program shader error occurred
 */
export default class WebGLProgramShaderError extends Error {
    /**
     * Create new instance
     * @param {string} message
     */
    constructor(message) {
        super(`WebGL program shader error: ${message}`);
    }
}
