import CHART_TYPES from '../plot/TypesEnum';

/**
 * MissingOrWrongChartTypeError
 * This class should be used to throw error when chart type is wrong
 */
export default class MissingOrWrongChartTypeError extends Error {
    /**
     * Create new instance
     * @param {*} mixed
     */
    constructor(mixed) {
      const acceptableValues = Array.from(CHART_TYPES.keys(), (value) => value);

      super(`Correct chart type expected ${String(acceptableValues)}, received: ${String(mixed)}`);
    }
}
