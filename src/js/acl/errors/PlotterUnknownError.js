/**
 * PlotterUnknownError
 * This class should be used to throw error when type of chart has no defined Plotter class
 */
export default class PlotterUnknownError extends Error {
    /**
     * Create new instance
     * @param {*} type
     */
    constructor(type) {
        super(`Unknown Plotter for type of chart: ${String(type)}`);
    }
}
