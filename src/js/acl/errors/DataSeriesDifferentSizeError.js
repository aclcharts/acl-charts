/**
 * DataSeriesDifferentSizeError
 * Threw when data series are different sizes
 */
export default class DataSeriesDifferentSizeError extends Error {
    /**
     * Create new instance
     */
    constructor() {
        super(`Data series have different size. Same size is required`);
    }
}
