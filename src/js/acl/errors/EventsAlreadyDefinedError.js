/**
 * EventsAlreadyDefinedError
 * This class should be used to throw error when Events already defined
 */
export default class EventsAlreadyDefinedError extends Error {
    /**
     * Create new instance
     */
    constructor() {
        super(`Events already defined`);
    }
}
