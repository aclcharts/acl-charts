/**
 * InstantiateStaticError
 * This class should be used to prevent instantiate static class
 */
export default class InstantiateStaticError extends Error {
    /**
     * Create new instance
     * @param {class} prototype
     */
    constructor(prototype) {
        super(`Cannot instantiate static class: ${prototype.name}`);
    }
}
