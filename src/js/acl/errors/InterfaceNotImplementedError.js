/**
 * InterfaceNotImplementedError
 * This class should be used to throw error when target not implements given interface
 */
export default class InterfaceNotImplementedError extends Error {
    /**
     * Create new instance
     * @param {string} targetClass
     * @param {string} interfaceClass
     */
    constructor(targetClass, interfaceClass) {
        super(`Interface implementation error: ${targetClass} not implements ${interfaceClass}`);
    }
}