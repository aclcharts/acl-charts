/**
 * NotImplementedError
 * This class should be used to signal not implemented methods
 */
export default class NotImplementedError extends Error {
    /**
     * Create new instance
     */
    constructor() {
        super('Method not implemented');
    }
}
