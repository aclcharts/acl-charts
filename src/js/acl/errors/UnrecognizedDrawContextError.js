/**
 * UnrecognizedDrawContextError
 * This class should be used to throw error if draw context is unrecognized
 */
export default class UnrecognizedDrawContextError extends Error {
    /**
     * Create new instance
     * @param {*} context
     */
    constructor(context) {
        super(`Unrecognized draw context: ${String(context)}`);
    }
}
