import ChartManager from './ChartManager';

/**
 * CharManagers private field
 * @type {WeakMap}
 * @private
 */
const _chartManagers = new WeakMap();

/**
 * ChartFacade class
 * Exposes API
 */
export default class ChartFacade {
    /**
     * Create ChartFacade instance
     * @param {object} params
     */
    constructor(params) {
        _chartManagers.set(this, new ChartManager(params));
    }

    /**
     * Get settings with proxy interface
     * @returns {Proxy}
     */
    getSettings() {
        return _chartManagers.get(this).settingsManager.settings;
    }

    /**
     * Get settings via property
     * @returns {Proxy}
     */
    get settings() {
        return this.getSettings();
    }

    /**
     * Get data with proxy interface
     * @returns {Proxy}
     */
    getData() {
        return _chartManagers.get(this).dataManager.data;
    }

    /**
     * Get data via property
     * @returns {Proxy}
     */
    get data() {
        return this.getData();
    }
}
