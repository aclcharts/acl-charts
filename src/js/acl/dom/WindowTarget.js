import DomManager from './DomManager';
import NotInstanceOfError from '../errors/NotInstanceOfError';

/**
 * DomManager private field
 * @type {WeakMap<object,DomManager>}
 * @private
 */
const _domManager = new WeakMap();

/**
 * Current state of window resize listener
 * @type {WeakMap<object,state>}
 */
const _state = new WeakMap();

/**
 * Callback encapsulating function
 * @type {WeakMap<object,Function>}
 * @private
 */
const _callback = new WeakMap();

/**
 * Timeout variable
 * @type {WeakMap<object,number>}
 * @private
 */
const _timeout = new WeakMap();

/**
 * Delay in milliseconds
 * @type {number}
 */
const delay = 50;

/**
 *
 * @type {{resize: string}}
 */
const events = {
    resize: 'resize',
};

/**
 * WindowTarget class
 * Responsible for interacting with window object
 */
export default class WindowTarget {
    /**
     * Create instance
     * @param domManager
     */
    constructor(domManager) {
        if (domManager instanceof DomManager === false) {
            throw new NotInstanceOfError(DomManager);
        }
        _domManager.set(this, domManager);
    }

    /**
     * Set window resize listener state (true - enable, false - disable)
     * @param {bool} state
     */
    setWindowResizeListenerState(state) {
        if (!_callback.get(this)) {
            _callback.set(this, (event) => {
                this.onResize(this, event);
            });
        }

        if (state !== _state.get(this)) {
            _state.set(this, state);
        }

        if (state) {
            window.addEventListener(events.resize, _callback.get(this));
        } else {
            window.removeEventListener(events.resize, _callback.get(this));
        }
    }

    /**
     * OnResize callback
     * Used if window was resized. If yes, then change settings (width, height)
     * and redraw chart
     * @param {WindowTarget} parent
     * @param {Event} event
     */
    onResize(parent, event) {
        clearTimeout(_timeout.get(parent));
        _timeout.set(this, setTimeout(() => {
            const domManager = _domManager.get(parent),
                element = domManager.getElement(),
                settings = domManager.getChartManager().getSettingsManager().settings;

            settings.width = element.clientWidth;
            settings.height = element.clientHeight;
        }, delay));
    }
}
