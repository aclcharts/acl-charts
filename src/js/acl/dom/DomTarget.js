import DomManager from './DomManager';
import NotInstanceOfError from '../errors/NotInstanceOfError';

/**
 * DomManagers private field
 * @type {WeakMap<object,DomManager>}
 * @private
 */
const _domManagers = new WeakMap();

/**
 * Elements private field
 * @type {WeakMap<object,Element>}
 * @private
 */
const _elements = new WeakMap();

/**
 * DomTarget class
 * Responsive for handling target for chart
 */
export default class DomTarget {
    /**
     * Create instance
     * @param {DomManager} domManager
     */
    constructor(domManager) {
        if (domManager instanceof DomManager === false) {
            throw new NotInstanceOfError(DomManager);
        }
        
        const settingsManager = domManager.chartManager.settingsManager;
        const {target, width, height} = settingsManager.settings;

        _domManagers.set(this, domManager);
        _elements.set(this, this.findTarget(target));

        if (width !== undefined) {
            this.setWidth(width);
        }

        if (height !== undefined) {
            this.setHeight(height);
        }
    }

    /**
     * Find target
     * Find Element from current DOM, that is target for chart rendering.
     * @param {string|Element} target
     * @returns {Element}
     */
    findTarget(target) {
        if (target === undefined) {
            throw 'DomTarget Error: element settings value doesn\'t exist';
        }

        if (target instanceof Element) {
            return target;
        }

        if ( typeof target === 'string') {
            const element = document.querySelector(target);

            if (element instanceof Element) {
                return element;
            }
        }

        throw 'DomTarget Error: element doesn\'t exist';
    }

    /**
     * Get Element
     * @returns {Element}
     */
    getElement() {
        return _elements.get(this);
    }

    /**
     * get element via property
     * @returns {Element}
     */
    get element() {
        return this.getElement();
    }

    /**
     * Set Element
     * @param {Element} element
     * @returns {DomTarget}
     */
    setElement(element) {
        _elements.set(this, element);

        return this;
    }

    /**
     * Set width of Element
     * @param {number} width
     * @returns {DomTarget}
     */
    setWidth(width) {
        this.getElement().style.width = width;

        return this;
    }

    /**
     * Get width of element
     * @returns {number}
     */
    getWidth() {
        return this.getElement().clientWidth;
    }

    /**
     * Get width via property
     * @returns {number}
     */
    get width() {
        return this.getWidth();
    }

    /**
     * Set height of Element
     * @param {number} height
     * @returns {DomTarget}
     */
    setHeight(height) {
        this.getElement().style.height = height;

        return this;
    }

    /**
     * Get height of element
     * @returns {number}
     */
    getHeight() {
        return this.getElement().clientHeight;
    }

    /**
     * Get height via property
     * @returns {number}
     */
    get height() {
        return this.getHeight();
    }
}
