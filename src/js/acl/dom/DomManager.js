import ChartManager from '../ChartManager';
import DomTarget from './DomTarget';
import WindowTarget from './WindowTarget';
import NotInstanceOfError from '../errors/NotInstanceOfError';
import BaseClass from '../BaseClass';
import EventsMixin from '../mixins/EventsMixin';

/**
 * ChartManagers private field
 * @type {WeakMap<object,ChartManager>}
 * @private
 */
const _chartManagers = new WeakMap();

/**
 * DomTarget private field
 * @type {WeakMap<object,DomTarget>}
 * @private
 */
const _domTargets = new WeakMap();

/**
 * WindowTarget private field
 * @type {WeakMap<object,WindowTarget>}
 * @private
 */
const _windowTargets = new WeakMap();
/**
 * EventHandlers container
 * @type {WeakMap<object,EventsHandler>}
 * @private
 */
const _eventHandlers = new WeakMap();

/**
 * Events definitions
 * @type {{}}
 */
const events = {
    resized: Symbol('resized'),
};

/**
 * Callbacks definitions
 * @type {{}}
 */
const callbacks = {
    settings: {
        changed: (data, owner) => {
            const domTarget = _domTargets.get(owner);
            const windowTarget = _windowTargets.get(owner);

            let resized = false;

            switch (data.property) {
                case 'width':
                    domTarget.setWidth(data.value);
                    resized = true;
                    break;

                case 'height':
                    domTarget.setHeight(data.value);
                    resized = true;
                    break;

                case 'updateOnWindowResize':
                    windowTarget.setWindowResizeListenerState(data.value);
                    break;
            }

            resized && _eventHandlers.get(owner).trigger(events.resized, {
                    width: domTarget.width,
                    height: domTarget.height
            });
        }
    }
};

/**
 * DomManager class
 * Manages dom handlers like ex. DomTarget
 */
export default class DomManager extends EventsMixin(BaseClass) {
    /**
     * Create instance
     * @param {ChartManager} chartManager
     */
    constructor(chartManager) {
        super();

        if (chartManager instanceof ChartManager === false) {
            throw new NotInstanceOfError(ChartManager);
        }

        // Initialize events
        _eventHandlers.set(this, this.initEvents(events));

        const settingsManager = chartManager.settingsManager;
        const settings = settingsManager.settings;

        // Set callback on settings event
        settingsManager.on(settingsManager.events.changed, (data) => {
            callbacks.settings.changed(data, this);
        });

        _chartManagers.set(this, chartManager);
        _domTargets.set(this, new DomTarget(this));
        _windowTargets.set(this, new WindowTarget(this));

        let state = null;

        if (state = settingsManager.settings.updateOnWindowResize) {
            _windowTargets.get(this).setWindowResizeListenerState(state);
        }
    }

    /**
     * Get ChartManager
     * Get associated ChartManager instance
     * @returns {ChartManager}
     */
    getChartManager() {
        return _chartManagers.get(this);
    }

    /**
     * Get ChartManager via property
     * @returns {ChartManager}
     */
    get chartManager() {
        return this.getChartManager();
    }

    /**
     * Get dom Element
     * @returns {Element}
     */
    getElement() {
        return _domTargets.get(this).element;
    }

    /**
     * Get dom Element via property
     * @returns {Element}
     */
    get element() {
        return this.getElement();
    }
}
