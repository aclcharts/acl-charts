import BaseClass from '../BaseClass';
import NotArrayError from '../errors/NotArrayError';
import NotLiteralObjectError from '../errors/NotLiteralObjectError';
import MissingOrWrongChartTypeError from '../errors/MissingOrWrongChartTypeError';
import MissingOrWrongChartContentError from '../errors/MissingOrWrongChartContentError';
import EventsMixin from '../mixins/EventsMixin';

/**
 * Data container
 * @type {WeakMap<object,array>}
 * @private
 */
const _data = new WeakMap();

/**
 * Proxies container
 * @type {WeakMap<object,proxy>}
 * @private
 */
const _proxies = new WeakMap();

/**
 * EventHandlers container
 * @type {WeakMap<object,EventsHandler>}
 * @private
 */
const _eventHandlers = new WeakMap();

/**
 * Events definitions
 * @type {{}}
 */
const events = {
    changed: Symbol('changed')
};

/**
 * Data class
 */
export default class DataManager extends EventsMixin(BaseClass) {
    /**
     * Create instance
     * @param {{}} [data=[]] - source of data as literal object
     */
    constructor(data = []) {
        super();

        // Check if data is type of array object
        if (!Array.isArray(data)) {
            throw new NotArrayError(data);
        }

        // Store data array
        _data.set(this, data);

        // init events
        _eventHandlers.set(this, this.initEvents(events));

        // Create proxy on data structure
        _proxies.set(this, new Proxy(data, {
            set: (object, property, value) => {
                object[property] = value;

                _eventHandlers.get(this).trigger(events.changed, value);
                return true;
            }
        }));
    }

    /**
     * Get data via proxy (for reactivity)
     * @param {bool} [proxy=true] By default return proxy instead
     * @returns {Proxy}
     */
    getData(proxy = true) {
        return proxy ? _proxies.get(this) : _data.get(this);
    }

    get data() {
        return this.getData();
    }
}
