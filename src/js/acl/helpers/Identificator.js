import InstantiateAbstractError from '../errors/InstantiateStaticError';

/**
 * Incremented ids private static field
 * @type {Map}
 * @private
 */
const _ids = new Map();
const _uids = new Map();

/**
 * Identificator static class
 * Helper for generating id and uid
 */
export default class Identificator {
    /**
     * Create new Identificator instance
     */
    constructor() {
        throw new InstantiateAbstractError(Identificator);
    }

    /**
     * Get Id
     * @param {string} [context=null] - for scoping generated ids
     * @returns {number}
     */
    static getId(context = null) {
        if (!_ids.has(context)) {
            _ids.set(context, 0);
        }

        const id = _ids.get(context) + 1;
        _ids.set(context, id);

        return id;
    }

    /**
     * Get UId
     * @param {string} [context=null] - for scoping generated uids
     * @returns {number}
     */
    static getUId(context = null) {
        if (!_uids.has(context)) {
            _uids.set(context, new Set());
        }

        const uids = _uids.get(context);
        let uid;

        while (true) {
            uid = Math.floor(1 + Math.random() * 10000000000);

            if (!uids.has(uid)) {
                uids.add(uid);
                return uid;
            }
        }
    }
}
