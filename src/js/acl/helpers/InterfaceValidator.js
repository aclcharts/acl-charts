import InterfaceNotFullyImplementedError from '../errors/InterfaceNotFullyImplementedError';
import InterfaceNotImplementedError from '../errors/InterfaceNotImplementedError';

/**
 * Validate interface
 * Used for check if interface methods was fully implemented
 * @param {object} target
 * @param {class} interfaceClass
 */
export default function validateInterface(target, interfaceClass) {
    // Check if target generally implements interface
    if (target instanceof interfaceClass === false) {
        throw new InterfaceNotImplementedError(
            target.constructor.name,
            interfaceClass.constructor.name
        );
    }

    const targetMethods = new Set();
    let prototype = target;

    // Recursively grab all methods from prototypes during interfaceClass is occurred
    while (prototype = Reflect.getPrototypeOf(prototype)) {
        if (prototype.constructor === interfaceClass) {
            break;
        }

        // Filter properties to functions only
        Reflect.ownKeys(prototype).map(
            (key) => prototype[key] instanceof Function && targetMethods.add(key)
        );
    }

    // Grab functions of interface class
    const interfacePrototype = interfaceClass.prototype;
    const interfaceMethods = Reflect.ownKeys(interfacePrototype)
        .filter((key) => interfacePrototype[key] instanceof Function);

    // Check if all functions was implemented
    if (interfaceMethods.every((method) => targetMethods.has(method)) === false) {
        throw new InterfaceNotFullyImplementedError(
            target.constructor.name,
            interfaceClass.constructor.name
        );
    }
}
