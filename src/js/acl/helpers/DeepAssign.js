/**
 * DeepAssign class
 * Responsible for deep assign objects (like Object.assign, but recursive).
 */
export default class DeepAssign {
    /**
     * Assign source properties to target
     * @param {{}} target
     * @param {{}} source
     * @returns {{}}
     */
    static assign(target, source) {
        const keys = Reflect.ownKeys(source);
        const merged = Object.assign({}, target);

        for (let key of keys) {
            if (source[key] !== null && source[key].constructor === {}.constructor) {
                if (merged[key] === undefined) {
                    merged[key] = Object.assign({}, source[key]);
                } else {
                    merged[key] = DeepAssign.assign(merged[key], source[key]);
                }
            } else if (merged[key] === undefined) {
                merged[key] = source[key];
            }
        }

        return merged;
    }
}
