import AbstractPlotter from './AbstractPlotter';

/**
 * TestPlotter
 *
 * Plotting test screen (use this for testing plotting library drawing functions implementation)
 * @implements PlotterInterface
 */
export default class TestPlotter extends AbstractPlotter {
    /**
     * Plot test
     * @returns {TestPlotter}
     */
    plot() {
        this.getContext()
            .setClearColor(255, 255, 255, 1)
            .clear()
            .setFillColor(255, 100, 100, 1)
            .setLineWidth(4)
            .setStrokeColor(0, 0, 0, 1)
            .setStroke(true)
            .setFill(false)
            .drawRectangle(10, 10, 60, 60)
            .setFillColor(0, 0, 255, 1)
            .setFill(true)
            .drawRectangle(20, 20, 40, 40)
            .setStrokeColor(0, 150, 0, 1)
            .setLineWidth(1)
            .drawLine(230, 10, 330, 110)
            .setLineWidth(3)
            .drawLine(250, 10, 350, 110)
            .setLineWidth(5)
            .drawLine(270, 10, 370, 110)
            .setStrokeColor(255, 0, 0, 1)
            .drawMultiLine([230,140, 260, 120, 230, 100, 245, 60, 260, 80])
            .setFontSize(18)
            .setFontFamily('Arial')
            .setFillColor(255, 0, 200, 1)
            .drawText(400, 20, 'Test A')
            .setFontSize(14)
            .setFillColor(0, 0, 100, 1)
            .drawText(400, 40, 'Test B')
            .setFontSize(10)
            .setFillColor(0, 0, 0, 1)
            .drawText(400, 60, 'Test C')
            .setStrokeColor(0, 100, 0, 1)
            .setFillColor(0, 200, 250, 1)
            .setLineWidth(4.0)
            .drawPoly([10, 80, 30, 100, 45, 85, 60, 110, 80, 95, 80, 130, 10, 130])
            .setLineWidth(3)
            .setStrokeColor(64, 26, 95, 1)
            .setFillColor(150, 100, 180, 1)
            .setStroke(false)
            .drawCircle(160, 30, 6)
            .setLineWidth(5)
            .setStroke(true)
            .drawCircle(120, 30, 20)
            .setFill(false)
            .drawCircle(140, 90, 30)
            .setFill(true)
            .setStroke(false)
            .drawPie(560, 60, 40, Math.PI / 4, Math.PI / 2)
            .setFillColor(170, 120, 200, 1)
            .drawPie(560, 60, 50, Math.PI / 2, Math.PI * 0.75)
            .setFillColor(170, 120, 200, 1)
            .setLineWidth(2)
            .setStroke(true)
            .drawPie(560, 60, 60, Math.PI * 0.75, Math.PI)
            .setStrokeColor(64, 26, 95, 1)
            .setFillColor(150, 100, 180, 1)
            .setStroke(false)
            .drawArc(700, 60, 70, 20, Math.PI / 4, Math.PI / 2)
            .setFillColor(170, 120, 200, 1)
            .drawArc(700, 60, 80, 30, Math.PI / 2, Math.PI * 0.75)
            .setFillColor(170, 120, 200, 1)
            .setLineWidth(2)
            .setStroke(true)
            .drawArc(700, 60, 90, 40, Math.PI * 0.75, Math.PI);

        return this;
    }
}
