import AbstractPlotter from './AbstractPlotter';
import ColorConverter from './../../helpers/ColorConverter';
import LegendPlotter from './elements/LegendPlotter';
import GridPlotter from './elements/GridPlotter';
import DataSeriesDifferentSizeError from '../../errors/DataSeriesDifferentSizeError';

/**
 * Constants for defining if simple lines or polygon
 * @type {{Simple: Symbol, Polygon: Symbol}}
 */
const LineType = {
    Simple: Symbol('simple'),
    Polygon: Symbol('polygon')
};

/**
 * LinePlotter class
 * Responsible for generating line charts
 */
export default class LinePlotter extends AbstractPlotter {
    plot() {
        const data = this.getData(),
            settings = this.getSettings(),
            context = this.getContext();

        // Get context size
        const size = context.getSize();
        const shortestSize = Math.min(size.width, size.height);

        // Get line type
        const lineType = LineType.Simple;

        let max = 0;
        let min = 0;
        let frames = data[0].value.length - 1;

        data.forEach((data) => {
            if (data.value.length !== frames + 1) {
                throw new DataSeriesDifferentSizeError();
            }

            data.value.forEach((value) => {
                max = Math.max(max, value);
                min = Math.min(min, value);
            });
        });


        // Calculate area parameters
        const margin = {
            x: settings.line.marginX,
            y: settings.line.marginY,
        };

        const area = {
            x: Math.round(size.width * margin.x),
            y: Math.round(size.height * margin.y),
            width: Math.round(size.width - (size.width * margin.x * 2)),
            height: Math.round(size.height - (size.height * margin.y * 2))
        };

        // Calculate frame width
        let frameWidth = Math.round(area.width / frames);

        let delta = max - min;
            delta = delta === 0 ? 1 : delta;

        // Factor for scaling values to area height
        let factor = area.height / delta;

        // Get default colors palette
        const colors = settings.colors.palette;

        // Define font size
        let fontSize = settings.font.autoScale ? Math.round(10 + (shortestSize / 2) * settings.font.autoScaleFactor) : settings.font.size;
        fontSize = Math.max(settings.font.sizeMin, Math.min(settings.font.sizeMax, fontSize));

        // Stroke color
        const strokeColor = ColorConverter.toRGB(settings.colors.stroke);

        // Set default style
        context
            .setClearColor(ColorConverter.toRGB(settings.colors.background))
            .clear()
            .setTextAlign('middle')
            .setTextBaseline('middle')
            .setFontSize(fontSize)
            .setFontFamily(settings.font.family)
            .setFill(true)
            .setStroke(settings.appearance.stroke);

        const lines = data.length;
        let zeroOffset =  area.height + Math.round(factor * min);

        // Check if grid is enabled to show
        if (settings.grid.show === true) {
            const gridPlotter = new GridPlotter(this);
            const returned = gridPlotter.plot(min, max, area);

            // After Grid calculations, grab new factor and zero offset
            factor = returned.factor;
            zeroOffset = returned.zeroOffset;
        }

        // Draw zero line
        context.setLineWidth(settings.line.lineWidth)
            .setStrokeColor(strokeColor)
            .drawLine(area.x, area.y + zeroOffset, area.x + area.width, area.y + zeroOffset);

        // Render simple lines
        if (lineType === LineType.Simple) {
            const points = settings.line.points;
            const pointsSize = settings.line.pointsSize;

            data.forEach((data, index) => {
                data.value.forEach((value, frameIndex) => {
                    // Check if next data value is defined
                    if (frameIndex + 1 === data.value.length) {
                        return;
                    }

                    let x1, y1, x2, y2;
                    if (frameIndex !== frames) {
                        y1 = area.y + zeroOffset - Math.round(value * factor),
                            y2 = area.y + zeroOffset - Math.round(data.value[frameIndex + 1] * factor);

                        x1 = area.x + frameWidth * frameIndex;
                        x2 = area.x + frameWidth * (frameIndex + 1);
                    }

                    let color = ColorConverter.toRGB(colors[index % colors.length]);
                    context
                        .setStrokeColor(color)
                        .drawLine(x1, y1, x2, y2);

                    if (points) {
                        context.setFillColor(color)
                            .drawCircle(x1, y1, pointsSize)
                            .drawCircle(x2, y2, pointsSize);
                    }


                });
            });
        }

        // Prepare font style
        context.setTextAlign('middle');
        context.setTextBaseline('top');
        context.setFillColor(ColorConverter.toRGB(settings.colors.text));

        // Check if label size was defined
        if (settings.line.labelSize) {
            context.setFontSize(settings.line.labelSize);
        }

        const textFormatter = settings.line.textFormatter;

        // Render labels for simple lines
        if (lineType === LineType.Simple) {
            const labelAbove = settings.line.labelAbove;

            if (labelAbove) {
                context.setTextBaseline('top');
            } else {
                context.setTextBaseline('bottom');
            }

            data.forEach((data, index) => {
                data.value.forEach((value, frameIndex) => {
                    let x, y, text;

                    y = area.y + zeroOffset - Math.round(value * factor) + 6;
                    x = area.x + frameWidth * frameIndex;

                    text = textFormatter(index, data.label, value);

                    if (labelAbove) {
                        context.drawText(x, y - 10, text);
                        return;
                    }

                    context.drawText(x, y, text);
                });
            });
        }

        // Create legend plotter
        const legendPlotter = new LegendPlotter(this);
        legendPlotter.plot({fontSize, colors, strokeColor, totalValue: null});

        // Render frame labels if defined
        if (settings.line.frames.labels) {
            const textFormatter = settings.line.frames.textFormatter,
                labels = settings.line.frames.labels,
                labelAbove = settings.line.frames.labelAbove,
                labelBottom = settings.line.frames.labelBottom;

            if (labels.length !== frames + 1) {
                return;
            }

            context.setTextBaseline('top')
                .setTextAlign('middle');

            let y = area.y + zeroOffset + 5;

            if (labelAbove) {
                y -= 5;
                context.setTextBaseline('bottom');
            }

            if (labelBottom) {
                y = area.y + area.height + 5;
                context.setTextBaseline('top');
            }

            if (settings.line.frames.labelSize) {
                context.setFontSize(settings.line.frames.labelSize);
            }

            labels.forEach((label, index) => {
                let text = textFormatter(index, label),
                    x = area.x + frameWidth * index;

                context.drawText(x, y, text);
            });
        }

        return this;
    }
}