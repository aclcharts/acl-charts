import NotImplementedError from '../../errors/NotImplementedError';
import InstantiateInterfaceError from '../../errors/InstantiateInterfaceError';

/**
 * PlotterInterface
 * @interface
 */
export default class PlotterInterface {
    /**
     * Create Plotter
     */
    constructor() {
        if (this.constructor === PlotterInterface) {
            throw new InstantiateInterfaceError(PlotterInterface);
        }
    }

    /**
     * Set settings
     * @abstract
     * @param {{}} settings
     */
    setSettings(settings) {
        throw new NotImplementedError();
    }

    /**
     * Get settings
     * @abstract
     */
    getSettings() {
        throw new NotImplementedError();
    }

    /**
     * Set data
     * @param {Array} data
     */
    setData(data) {
        throw new NotImplementedError();
    }

    /**
     * Get data
     */
    getData() {
        throw new NotImplementedError();
    }

    /**
     * Set context
     * @param {ContextInterface} context
     */
    setContext(context) {
        throw new NotImplementedError();
    }

    /**
     * Get context
     */
    getContext() {
        throw new NotImplementedError();
    }

    /**
     * Set size of plotter
     * @param {number} width
     * @param {number} height
     */
    setSize(width, height) {
        throw new NotImplementedError();
    }

    /**
     * Plot chart
     * @abstract
     */
    plot() {
        throw new NotImplementedError();
    }
}