import AbstractPlotter from './AbstractPlotter';
import ColorConverter from './../../helpers/ColorConverter';
import LegendPlotter from './elements/LegendPlotter';

/**
 * DonutPlotter
 *
 * Plotting donut charts
 * @implements PlotterInterface
 */
export default class DonutPlotter extends AbstractPlotter {
    /**
     * Plot chart
     * @returns {DonutPlotter}
     */
    plot() {
        const data = this.getData(),
            settings = this.getSettings(),
            context = this.getContext();

        // Sum of all values
        let total = 0;

        // Current radian
        let radian = 0;

        // Size of context etc.
        const size = context.getSize();
        const shortestSize = Math.min(size.width, size.height);
        const radius = (shortestSize * settings.donut.radius) / 2;

        // Get center of context
        const center = {
            x: size.width * 0.5,
            y: size.height * 0.5
        };

        // Calculate total
        data.forEach((data) => {
            total += data.value;
        });

        // Get default colors palette
        const colors = settings.colors.palette;

        // Define font size
        let fontSize = settings.font.autoScale ? Math.round(10 + radius * settings.font.autoScaleFactor) : settings.font.size;
        fontSize = Math.max(settings.font.sizeMin, Math.min(settings.font.sizeMax, fontSize));

        // Stroke color
        const strokeColor = ColorConverter.toRGB(settings.colors.stroke);

        // Text formatter
        const textFormatter = settings.donut.textFormatter;

        // Set default style
        context
            .setClearColor(ColorConverter.toRGB(settings.colors.background))
            .clear()
            .setLineWidth(settings.appearance.strokeWidth)
            .setStrokeColor(strokeColor)
            .setTextAlign('middle')
            .setTextBaseline('middle')
            .setFontSize(fontSize)
            .setFontFamily(settings.font.family)
            .setFill(true)
            .setStroke(settings.appearance.stroke);

        // Render all data
        data.forEach((data, index) => {
            let percent = data.value / total;
            let range = Math.PI * percent * 2;

            let tx = center.x + Math.cos (radian + range / 2) * (radius - radius / settings.donut.radiusDivider),
                ty = center.y + Math.sin (radian + range / 2) * (radius - radius / settings.donut.radiusDivider);

            let shiftX = Math.cos(radian + range /2) * shortestSize * settings.donut.shift;
            let shiftY = Math.sin(radian + range /2) * shortestSize * settings.donut.shift;
            let text = textFormatter(index, data.label, data.value, Math.round(percent * 100), total);

            // Draw donut
            context
                .setFillColor(ColorConverter.toRGB(colors[index % colors.length]))
                .drawArc(center.x + shiftX, center.y + shiftY, radius, radius / settings.donut.radiusDivider, radian, radian + range)
                .setFillColor(ColorConverter.toRGB(settings.colors.text))
                .drawText(tx + shiftX, ty + shiftY, text);

            radian += range;
        });

        // Create element plotter
        const legendPlotter = new LegendPlotter(this);
        legendPlotter.plot({fontSize, colors, strokeColor, totalValue: total});

        return this;
    }
}
