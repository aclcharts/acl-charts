import AbstractPlotter from './../AbstractPlotter';
import ColorConverter from './../../../helpers/ColorConverter';

/**
 * Parent plotter private field
 * @type {WeakMap<object,PlotterInterface>}
 * @private
 */
const _parentPlotter = new WeakMap();

/**
 * GridPlotter class
 * Plotter for generating grid
 */
export default class GridPlotter extends AbstractPlotter {
    /**
     * Create instance
     * @param {PlotterInterface} parentPlotter
     */
    constructor(parentPlotter) {
        super();

        _parentPlotter.set(this, parentPlotter);
    }

    /**
     * Plot grid
     * @param {number} min - minimal value in data
     * @param {number} max - maximal value in data
     * @param {x,y,width,height} area - area for plotting grid
     * @returns {{factor: number, zeroOffset: number}} - recalculated factor and zero offset (offset Y to zero line)
     */
    plot(min = 0, max = 0, area = {x:0, y:0, width: 1, height: 1}) {
        let parentPlotter = _parentPlotter.get(this),
            context = parentPlotter.getContext(),
            settings = parentPlotter.getSettings(),
            minimum,
            exponent,
            magnitude,
            residual,
            tickHeight,
            tickSize = settings.grid.axisY.tickSize,
            maxTicks = settings.grid.axisY.maxTicks,
            maxTickValue,
            ticksCounter,
            ticksToRender,
            multiplier = 1,
            cells = Math.round(area.height / tickSize) || 1,
            gridColor = ColorConverter.toRGB(settings.grid.color),
            textFormatter = settings.grid.axisY.textFormatter,
            fontSize = context.getFontSize(),
            yOffset;

        // Check if grid is disabled
        if (settings.grid.show === false) {
            return;
        }

        // If computed number of cells is smaller than 2,
        // then set cells to 2 and recalculate minCellSize
        if (cells < 2) {
            cells = 2;
            tickSize = area.height / 2;
        }

        // If max ticks was defined,
        // then recalculate tickSize and cells
        if (maxTicks) {
            // If there is negative number, then add additional tick
            cells = Math.max(min < 0 ? 2 : 1, Math.ceil(maxTicks) - 2);
            tickSize = Math.round((area.height / cells)) || 1;
        }

        // Get range of values, exponent of range and magnitude
        // for calculating cell sizes
        minimum = Math.abs(max - min) / cells;
        exponent = Math.floor(Math.log(minimum) / Math.LN10);
        magnitude = Math.pow(10, exponent);
        residual = minimum / magnitude;

        // Compute multiplier for magnitude
        if (residual > 5) {
            multiplier = 10;
        } else if (residual > 2) {
            multiplier = 5;
        } else if (residual > 1) {
            multiplier = 2;
        }

        // Compute tickSize
        tickHeight = multiplier * magnitude;

        // Set some styles
        context.setLineWidth(settings.grid.lineWidth)
            .setStrokeColor(gridColor)
            .setTextAlign('end')
            .setTextBaseline('middle')
            .setFillColor(gridColor);


        // recalculate tickSize to stretch ticks to whole area
        maxTickValue = Math.ceil(max / tickHeight) * tickHeight;
        ticksCounter = Math.ceil(max / tickHeight) + Math.ceil(-min / tickHeight);
        ticksToRender = ticksCounter;
        tickSize *= Math.round(area.height / ticksToRender) / tickSize;

        // Check if label size was defined
        if (settings.bar.labelSize) {
            context.setFontSize(settings.bar.labelSize);
        }

        // If font size is too big, then rescale it
        if (fontSize >= tickSize) {
            context.setFontSize(tickSize - 4);
        }

        // Render lines and texts
        while (ticksToRender >= 0) {
            yOffset = ticksToRender * tickSize;
            context.drawLine(area.x, area.y + yOffset, area.x + area.width, area.y + yOffset)
                .drawText(area.x - 10, area.y + yOffset, textFormatter(maxTickValue - (ticksToRender * tickHeight)));
            ticksToRender -= 1;
        }

        // Return new factor and zeroOffset
        return {
            factor: area.height / (ticksCounter * tickHeight),
            zeroOffset: Math.ceil(max / tickHeight) * tickSize
        };
    }
}