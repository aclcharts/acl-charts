import AbstractPlotter from './../AbstractPlotter';
import ColorConverter from './../../../helpers/ColorConverter';

/**
 * Parent plotter private field
 * @type {WeakMap<object,PlotterInterface>}
 * @private
 */
const _parentPlotter = new WeakMap();

/**
 * Plotter for generating legend
 */
export default class LegendPlotter extends AbstractPlotter {
    /**
     * Create instance
     * @param {PlotterInterface} parentPlotter
     */
    constructor(parentPlotter) {
        super();

        _parentPlotter.set(this, parentPlotter);
    }

    /**
     * Plot legend
     * @param {{}} params - additional params that were computed in parent plotter (fontSize, colors, strokeColor, totalValue)
     */
    plot(params) {
        const parentPlotter = _parentPlotter.get(this),
            context = parentPlotter.getContext(),
            settings = parentPlotter.getSettings(),
            data = parentPlotter.getData();

        // Check if legend is disabled
        if (settings.legend.show === false) {
            return;
        }

        // Grab params
        const {fontSize, colors, strokeColor, totalValue} = params;

        // Text formatter
        const textFormatter = settings.legend.textFormatter;

        // Draw legend
        context.setTextAlign('start')
            .setTextBaseline('middle')
            .setFontSize(fontSize * 0.8)
            .setStroke(settings.appearance.stroke)
            .setStrokeColor(strokeColor)
            .setFill(true);

        let rectX = 10;
        let textX = 16 + fontSize * 0.8;

        if (settings.legend.position === 'right') {
            let {width} = context.getSize();
            rectX = width - 10 - fontSize * 0.8;
            textX = width - 16 - fontSize * 0.8;
            context.setTextAlign('end');
        }

        // Plot legend
        data.forEach((data, index) => {
            const {r,g,b,a} = ColorConverter.toRGB(colors[index % colors.length]);

            const computed = Math.round((data.value / totalValue) * 100);
            const text = textFormatter(index, data.label, data.value, computed, totalValue);

            context.setFillColor(r,g,b,a)
                .drawRectangle(rectX, 10 + (index * fontSize), fontSize * 0.8, fontSize * 0.8)
                .setFillColor(ColorConverter.toRGB(settings.colors.text))
                .drawText(textX, 14 + index * fontSize, text);
        });
    }
}