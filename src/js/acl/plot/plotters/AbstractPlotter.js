import validateInterface from '../../helpers/InterfaceValidator';
import PlotterInterface from './PlotterInterface';

/**
 * Data container private field
 * @type {WeakMap<object,Proxy>}
 * @private
 */
const _data = new WeakMap();

/**
 * Settings container private field
 * @type {WeakMap<object,Proxy>}
 * @private
 */
const _settings = new WeakMap();

/**
 * Context container private field
 * @type {WeakMap<object,ContextInterface>}
 * @private
 */
const _context = new WeakMap();

/**
 * AbstractPlotter
 * base class for plotters
 * @implements PlotterInterface
 */
export default class AbstractPlotter extends PlotterInterface {
    /**
     * Create instance
     */
    constructor() {
        super();

        validateInterface(this, PlotterInterface);
    }

    /**
     * Set data
     * @param {Proxy} data
     * @returns {AbstractPlotter}
     */
    setData(data) {
        _data.set(this, data);

        return this;
    }

    /**
     * Get data
     * @returns {Proxy}
     */
    getData() {
        return _data.get(this);
    }

    /**
     * Get data via property
     * @returns {Proxy}
     */
    get data() {
        return this.getData();
    }

    /**
     * Set Settings
     * @param {Proxy} settings
     * @returns {AbstractPlotter}
     */
    setSettings(settings) {
        _settings.set(this, settings);

        return this;
    }

    /**
     * Get settings
     * @returns {Proxy}
     */
    getSettings() {
        return _settings.get(this);
    }

    /**
     * Get settings via property
     * @returns {Proxy}
     */
    get settings() {
        this.getSettings();
    }

    /**
     * Set drawing context
     * @param {ContextInterface} context
     * @returns {AbstractPlotter}
     */
    setContext(context) {
        _context.set(this, context);

        return this;
    }

    /**
     * Get drawing context
     * @returns {ContextInterface}
     */
    getContext() {
        return _context.get(this);
    }

    /**
     * Get drawing context via property
     * @returns {ContextInterface}
     */
    get context() {
        return this.getContext();
    }

    /**
     * Set size of context
     * @param {number} width
     * @param {number} height
     * @returns {AbstractPlotter}
     */
    setSize(width, height) {
        _context.get(this).setSize(width, height);

        return this;
    }
}
