import AbstractPlotter from './AbstractPlotter';
import ColorConverter from './../../helpers/ColorConverter';
import LegendPlotter from './elements/LegendPlotter';
import GridPlotter from './elements/GridPlotter';
import DataSeriesDifferentSizeError from '../../errors/DataSeriesDifferentSizeError';

/**
 * Constants for defining if bar is simple or complex
 * @type {{Simple: Symbol, Complex: Symbol}}
 */
const BarType = {
    Simple: Symbol('simple'),
    Complex: Symbol('complex')
};

/**
 * BarPlotter class
 * Responsible for generating bar charts
 */
export default class BarPlotter extends AbstractPlotter {
    plot() {
        const data = this.getData(),
            settings = this.getSettings(),
            context = this.getContext();

        // Get context size
        const size = context.getSize();
        const shortestSize = Math.min(size.width, size.height);

        // Get bar type
        const barType = (typeof data[0].value  === 'number') ? BarType.Simple : BarType.Complex;

        let max = 0;
        let min = 0;
        let frames = 1;

        // Get maximum and minimum value from simple bar type
        if (barType === BarType.Simple) {
            data.forEach((data) => {
                max = Math.max(max, data.value);
                min = Math.min(min, data.value);
            });
        }

        if (barType === BarType.Complex) {
            frames = data[0].value.length;

            data.forEach((data) => {
                if (data.value.length !== frames) {
                    throw new DataSeriesDifferentSizeError();
                }

                data.value.forEach((value) => {
                    max = Math.max(max, value);
                    min = Math.min(min, value);
                });
            });


        }

        // Calculate area parameters
        const margin = {
            x: settings.bar.marginX,
            y: settings.bar.marginY,
        };

        const area = {
            x: Math.round(size.width * margin.x),
            y: Math.round(size.height * margin.y),
            width: Math.round(size.width - (size.width * margin.x * 2)),
            height: Math.round(size.height - (size.height * margin.y * 2))
        };

        // Calculate frame width
        let frameWidth = Math.round(area.width / frames);

        let delta = max - min;
            delta = delta === 0 ? 1 : delta;

        // Factor for scaling values to area height
        let factor = area.height / delta;

        // Sum of all values
        let total = 0;
        let totals = [];

        // Calculate total
        if (barType === BarType.Simple) {
            data.forEach((data) => {
                total += data.value;
            });
        }

        // Get default colors palette
        const colors = settings.colors.palette;

        // Define font size
        let fontSize = settings.font.autoScale ? Math.round(10 + (shortestSize / 2) * settings.font.autoScaleFactor) : settings.font.size;
        fontSize = Math.max(settings.font.sizeMin, Math.min(settings.font.sizeMax, fontSize));

        // Stroke color
        const strokeColor = ColorConverter.toRGB(settings.colors.stroke);

        // Set default style
        context
            .setClearColor(ColorConverter.toRGB(settings.colors.background))
            .clear()
            .setTextAlign('middle')
            .setTextBaseline('middle')
            .setFontSize(fontSize)
            .setFontFamily(settings.font.family)
            .setFill(true)
            .setStroke(settings.appearance.stroke);

        const barWidth = Math.min(1, Math.max(settings.bar.barWidth, 0));
        const bars = data.length;
        let zeroOffset =  area.height + Math.round(factor * min);

        // Check if grid is enabled to show
        if (settings.grid.show === true) {
            const gridPlotter = new GridPlotter(this);
            const returned = gridPlotter.plot(min, max, area);

            // After Grid calculations, grab new factor and zero offset
            factor = returned.factor;
            zeroOffset = returned.zeroOffset;
        }

        // Draw zero line
        context.setLineWidth(settings.appearance.strokeWidth)
            .setStrokeColor(strokeColor)
            .drawLine(area.x, area.y + zeroOffset, area.x + area.width, area.y + zeroOffset);


        // Render bars for simple type
        if (barType === BarType.Simple) {
            let width = (frameWidth * settings.bar.frameWidth / bars),
                frameMarginOffset = frameWidth * (1 - settings.bar.frameWidth) / 2,
                computedBarWidth = width - width * (1 - barWidth),
                computedBarOffset = (width * (1 - barWidth) / 2) + frameMarginOffset;

            data.forEach((data, index) => {
                let height = Math.round(data.value * factor),
                    x = area.x + (width * index),
                    y = area.y + zeroOffset - height;

                context
                    .setFillColor(ColorConverter.toRGB(colors[index % colors.length]))
                    .drawRectangle(x + computedBarOffset, y, computedBarWidth, height);
            });
        }

        // Render bars for complex type
        if (barType === BarType.Complex) {
            let width = (frameWidth * settings.bar.frameWidth / bars),
                frameMarginOffset = frameWidth * (1 - settings.bar.frameWidth) / 2,
                computedBarWidth = width - width * (1 - barWidth),
                computedBarOffset = (width * (1 - barWidth) / 2) + frameMarginOffset;

            data.forEach((data, index) => {
                data.value.forEach((value, frameIndex) => {
                    let height = Math.round(value * factor),
                        x = area.x + (width * index) + frameIndex * frameWidth,
                        y = area.y + zeroOffset - height;

                    context
                        .setFillColor(ColorConverter.toRGB(colors[index % colors.length]))
                        .drawRectangle(x + computedBarOffset, y, computedBarWidth, height);
                });
            });
        }

        // Prepare font style
        context.setTextAlign('middle');
        context.setTextBaseline('top');
        context.setFillColor(ColorConverter.toRGB(settings.colors.text));

        // Check if label size was defined
        if (settings.bar.labelSize) {
            context.setFontSize(settings.bar.labelSize);
        }

        const textFormatter = settings.bar.textFormatter;

        // Render labels for simple bar type
        if (barType === BarType.Simple) {
            let width = (frameWidth * settings.bar.frameWidth / bars),
                frameMarginOffset = frameWidth * (1 - settings.bar.frameWidth) / 2;

            const labelOutside = settings.bar.labelOutside;
            const labelAbove = settings.bar.labelAbove;

            if (labelAbove) {
                context.setTextBaseline('bottom');
            }

            data.forEach((data, index) => {
                let percent = data.value / total,
                    height = Math.round(data.value * factor),
                    x = area.x + ((width * index)) + width / 2 + frameMarginOffset,
                    y = area.y + zeroOffset + 10,
                    text = textFormatter(index, data.label, data.value, Math.round(percent * 100), total);

                if (labelAbove) {
                    if (data.value > 0) {
                        y -= height + 10;
                        context.drawText(x, y, text);
                    } else {
                        context.drawText(x, y - 10, text);
                    }
                    return;
                }

                if (labelOutside) {
                    if (data.value < 0) {
                        context.setTextBaseline('bottom')
                            .drawText(x, y - 15, text)
                            .setTextBaseline('top');
                    } else {
                        context.drawText(x, y, text);
                    }
                    return;
                }
                context.drawText(x, y, text);
            });
        }

        // Render labels for complex bar type
        if (barType === BarType.Complex) {
            let width = (frameWidth * settings.bar.frameWidth / bars),
                frameMarginOffset = frameWidth * (1 - settings.bar.frameWidth) / 2;

            const labelOutside = settings.bar.labelOutside;
            const labelAbove = settings.bar.labelAbove;

            if (labelAbove) {
                context.setTextBaseline('bottom');
            }

            data.forEach((data, index) => {
                data.value.forEach((value, frameIndex) => {
                    let height = Math.round(value * factor),
                        x = area.x + (width * index) + frameIndex * frameWidth + width / 2 + frameMarginOffset,
                        y = area.y + zeroOffset + 10,
                        text = textFormatter(index, data.label, value);

                    if (labelAbove) {
                        if (value > 0) {
                            y -= height + 10;
                            context.drawText(x, y, text);
                        } else {
                            context.drawText(x, y - 10, text);
                        }
                        return;
                    }

                    if (labelOutside) {
                        if (value < 0) {
                            context.setTextBaseline('bottom')
                                .drawText(x, y - 15, text)
                                .setTextBaseline('top');
                        } else {
                            context.drawText(x, y, text);
                        }
                        return;
                    }

                    context.drawText(x, y, text);
                });
            });
        }

        // Create legend plotter
        const legendPlotter = new LegendPlotter(this);
        legendPlotter.plot({fontSize, colors, strokeColor, totalValue: total});

        // Render frame labels if defined
        if (settings.bar.frames.labels) {
            const textFormatter = settings.bar.frames.textFormatter,
                labels = settings.bar.frames.labels,
                width = (frameWidth),
                labelAbove = settings.bar.frames.labelAbove,
                labelBottom = settings.bar.frames.labelBottom;

            if (labels.length !== frames) {
                return;
            }

            context.setTextBaseline('top')
                .setTextAlign('middle');

            let y = area.y + zeroOffset + 5;

            if (labelAbove) {
                y -= 5;
                context.setTextBaseline('bottom');
            }

            if (labelBottom) {
                y = area.y + area.height + 5;
                context.setTextBaseline('top');
            }

            if (settings.bar.frames.labelSize) {
                context.setFontSize(settings.bar.frames.labelSize);
            }

            labels.forEach((label, index) => {
                let x = area.x + ((width * index)) + width / 2,
                    text = textFormatter(index, label);

                context.drawText(x, y, text);
            });
        }

        return this;
    }
}