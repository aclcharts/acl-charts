/**
 * Test type for for testing drawing
 * @type {Symbol}
 */
export const Test = Symbol('Test');

/**
 * Pie chart type
 * @type {Symbol}
 */
export const Pie = Symbol('Pie');

/**
 * Donut chart type
 * @type {Symbol}
 */
export const Donut = Symbol('Donut');

/**
 * Bar type chart
 * @type {Symbol}
 */
export const Bar = Symbol('Bar');

/**
 * Line type chart
 * @type {Symbol}
 */
export const Line = Symbol('Line');