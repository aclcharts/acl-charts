import PlotterFactory from './PlotterFactory';

/**
 * ChartManagers private field
 * @type {WeakMap<object,ChartManager>}
 * @private
 */
const _chartManagers = new WeakMap();

/**
 * Plotter private field
 * @type {WeakMap<object,PlotterInterface>}
 * @private
 */
const _plotters = new WeakMap();

/**
 * PlotterManager class
 * Responsible for plotting chart on screen using drawing context
 */
export default class PlotterManager {
    /**
     * Create instance
     * @param {ChartManager} chartManager
     */
    constructor(chartManager) {
        // Store required objects
        _chartManagers.set(this, chartManager);

        const domManager = chartManager.domManager;
        const dataManager = chartManager.dataManager;

        // subscribe event of domManager (if size was changed, then redraw chart).
        domManager.on(domManager.events.resized, (data) => {
            plotter.setSize(data.width, data.height).plot();
        });

        // subscribe event of dataManager (if data was changed, then redraw chart).
        dataManager.on(dataManager.events.changed, (data) => {
           plotter.plot();
        });

        const data = chartManager.dataManager.data;
        const settings = chartManager.settingsManager.settings;

        const plotterFactory = new PlotterFactory();
        const plotter = plotterFactory.create(chartManager.settingsManager.settings.type);

        _plotters.set(this, plotter);

        plotter.setData(data)
            .setSettings(settings)
            .setContext(chartManager.drawingManager.context)
            .plot();
    }
}
