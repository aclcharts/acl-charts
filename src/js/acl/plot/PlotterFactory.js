import * as TypesEnum from './TypesEnum';
import PlotterUnknownError from '../errors/PlotterUnknownError';
import TestPlotter from './plotters/TestPlotter';
import PiePlotter from './plotters/PiePlotter';
import DonutPlotter from './plotters/DonutPlotter';
import BarPlotter from './plotters/BarPlotter';
import LinePlotter from './plotters/LinePlotter';

/**
 * PlotterFactory class
 * Responsible for creating plotters
 */
export default class PlotterFactory {
    /**
     * Create new Plotter
     * @param type
     * @returns {PlotterInterface}
     */
    create(type) {
        switch (type) {
            case TypesEnum.Test:
                return new TestPlotter();
                break;

            case TypesEnum.Pie:
                return new PiePlotter();
                break;

            case TypesEnum.Donut:
                return new DonutPlotter();
                break;

            case TypesEnum.Bar:
                return new BarPlotter();
                break;

            case TypesEnum.Line:
                return new LinePlotter();
                break;

            default:
                throw new PlotterUnknownError(type);
        }
    }
}
