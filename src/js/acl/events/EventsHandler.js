import EventAlreadyDefinedError from '../errors/EventAlreadyDefinedError';
import EventNotDefinedError from '../errors/EventNotDefinedError';
/**
 * Events private field
 * @type {WeakMap<object, Set>}
 * @private
 */
const _eventHandlers = new WeakMap();

/**
 * EventsHandler class
 * Responsible for handling custom events
 */
export default class EventsHandler {
    /**
     * Create new EventsHandler instance
     */
    constructor() {
        _eventHandlers.set(this, new Map());
    }

    /**
     * Get events names
     * @returns {Iterator.<string>}
     */
    getEvents() {
        return _eventHandlers.get(this).keys();
    }

    /**
     * Define event with given name
     * @param {string} name
     */
    define(name) {
        if (this.isDefined(name)) {
            throw new EventAlreadyDefinedError(name);
        }

        _eventHandlers.get(this).set(name, new Set());
    }

    /**
     * Check if event with given name is already defined
     * @param {string} name
     * @returns {boolean}
     */
    isDefined(name) {
        return (_eventHandlers.get(this).has(name));
    }

    /**
     * Get callbacks
     * @param name
     * @returns {Set}
     */
    getCallbacks(name) {
        if (!this.isDefined(name)) {
            throw new EventNotDefinedError(name);
        }

        return _eventHandlers.get(this).get(name);
    }

    /**
     * Trigger event with given name
     * and pass data to callbacks
     * @param {string} name
     * @param {{}} data
     */
    trigger(name, data) {
        this.getCallbacks(name).forEach((callback) => {
            callback(data);
        });
    }

    /**
     * Add callback to event
     * @param {string} name
     * @param {Function} callback
     */
    add(name, callback) {
        if (!this.isDefined(name)) {
            throw new EventNotDefinedError(name);
        }
        _eventHandlers.get(this).get(name).add(callback);
    }

    /**
     * Subscribe event
     * This is alias to 'add' method
     * @param {string} name
     * @param {Function} callback
     */
    subscribe(name, callback) {
        this.add(name, callback);
    }

    /**
     * Notify event
     * This is alias for 'trigger' method
     * @param {string} name
     * @param {{}} data
     */
    notify(name, data) {
        this.trigger(name, data);
    }

    /**
     * Get subscribers
     * This is alias for 'getCallbacks' method
     * @param name
     */
    getSubscribers(name) {
        this.getCallbacks(name);
    }
}
