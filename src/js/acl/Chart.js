import ChartFacade from './ChartFacade';
import Settings from './settings/SettingsManager';
import * as ContextsEnum from './graphic/draw/contexts/ContextEnum';
import * as TypesEnum from './plot/TypesEnum';

/**
 * Charts private field
 * @type {Set}
 * @private
 */
const _charts = new Set();

/**
 * Chart class
 * Exposed to user as main Chart class
 */
export default class Chart extends ChartFacade {
    /**
     * Create Chart instance
     * @param {object} params
     */
    constructor(params) {
        super(params);

        _charts.add(this);
    }

    /**
     * Get all Charts instances
     * @returns {Chart[]} all Charts
     */
    static getCharts() {
        return [..._charts];
    }

    /**
     * Get all available context options
     * @returns {{}} available contexts
     */
    static getContexts() {
        return ContextsEnum;
    }

    /**
     * Get all available char type options
     * @returns {{}} available
     */
    static getTypes() {
        return TypesEnum;
    }
}
