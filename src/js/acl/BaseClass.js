import Identificator from './helpers/Identificator';
import InstantiateAbstractError from './errors/InstantiateAbstractError';

/**
 * Identifiers private field
 * @type {WeakMap<object,{}>}
 * @private
 */
const _identifiers = new WeakMap();

/**
 * BaseClass
 * Used for give objects id, uid and toString method
 */
export default class BaseClass {
    /**
     * Create new BaseClass instance
     * @abstract
     */
    constructor() {
        if (this.constructor === BaseClass) {
            throw new InstantiateAbstractError(BaseClass);
        }

        const name = this.constructor.name,
            id  = Identificator.getId(name),
            uid = Identificator.getUId(name);

        _identifiers.set(this, {id, uid});
    }

    /**
     * Get Id
     * @returns {number} id
     */
    getId() {
        return _identifiers.get(this).id;
    }

    /**
     * Get Id via property
     * @returns {number} id
     */
    get id() {
        return this.getId();
    }

    /**
     * Get UId
     * @returns {number} uid
     */
    getUId() {
        return _identifiers.get(this).uid;
    }

    /**
     * Get UId via property
     * @returns {number} uid
     */
    get UId() {
        return this.getUId();
    }

    /**
     * Get name of class via instance
     * @returns {string} name
     */
    toString() {
        return this.constructor.name;
    }
}
