import BaseClass from './BaseClass';
import DataManager from './data/DataManager';
import SettingsManager from './settings/SettingsManager';
import DomManager from './dom/DomManager';
import DrawingManager from './graphic/draw/DrawingManager';
import PlotterManager from './plot/PlotterManager';

/**
 * SettingsManager private field
 * Stores SettingsManager objects
 * @type {WeakMap<object,SettingsManager>}
 * @private
 */
const _settingsManagers = new WeakMap();

/**
 * Data private field
 * Stores Data objects
 * @type {WeakMap<object,DataManager>}
 * @private
 */
const _dataManagers = new WeakMap();

/**
 * DomManagers private field
 * @type {WeakMap<object,DomManager>}
 * @private
 */
const _domManagers = new WeakMap();

/**
 * DrawingManagers private field
 * Stores DrawingManagers objects
 * @type {WeakMap<object,DrawingManager>}
 * @private
 */
const _drawingManagers = new WeakMap();

/**
 * PlotterManagers private field
 * Stores PlotterManagers objects
 * @type {WeakMap<object,PlotterManager>}
 * @private
 */
const _plotterManagers = new WeakMap();

/**
 * ChartManager class
 * Main class that init rest of acl classes
 */
export default class ChartManager extends BaseClass {
    /**
     * Create instance
     * @param {object} [settings={}] - chart settings
     * @param {object} [data={}] - chart data
     */
    constructor({settings = {}, data = {}} = {}) {
        super();

        _settingsManagers.set(this, new SettingsManager(settings));
        _dataManagers.set(this, new DataManager(data));
        _domManagers.set(this, new DomManager(this));
        _drawingManagers.set(this, new DrawingManager(this));
        _plotterManagers.set(this, new PlotterManager(this));
    }

    /**
     * Get DrawingManager
     * Get associated DrawingManager instance
     * @returns {DrawingManager}
     */
    getDrawingManager() {
        return _drawingManagers.get(this);
    }

    /**
     * Get DrawingManager via property
     * @returns {DrawingManager}
     */
    get drawingManager() {
        return this.getDrawingManager();
    }

    /**
     * Get DomManager
     * Get associated DomManager instance
     * @returns {DomManager}
     */
    getDomManager() {
        return _domManagers.get(this);
    }

    /**
     * Get DomManager via property
     * @returns {DomManager}
     */
    get domManager() {
        return this.getDomManager();
    }

    /**
     * Get SettingsManager
     * @returns {SettingsManager}
     */
    getSettingsManager() {
        return _settingsManagers.get(this);
    }

    /**
     * Get SettingsManager via property
     * @returns {SettingsManager}
     */
    get settingsManager() {
        return this.getSettingsManager();
    }

    /**
     * Get DataManager instance assigned to Chart
     * @returns {DataManager}
     */
    getDataManager() {
        return _dataManagers.get(this);
    }

    /**
     * Get DataManager via property
     * @returns {DataManager}
     */
    get dataManager() {
        return this.getDataManager();
    }

    /**
     * Get PlotterManager
     * @returns {PlotterManager}
     */
    getPlotterManager() {
        return _plotterManagers.get(this);
    }

    /**
     * Get PlotterManager via property
     * @returns {PlotterManager}
     */
    get plotterManager() {
        return this.getPlotterManager();
    }
}
