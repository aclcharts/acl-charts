import BaseClass from '../BaseClass';
import DeepAssign from '../helpers/DeepAssign';
import EventsMixin from '../mixins/EventsMixin';
import * as ContextEnum from '../graphic/draw/contexts/ContextEnum';
import NotLiteralObjectError from '../errors/NotLiteralObjectError';

/**
 * Settings container
 * @type {WeakMap<object,{}>}
 * @private
 */
const _settings = new WeakMap();

/**
 * Proxies container
 * @type {WeakMap<object,Proxy>}
 * @private
 */
const _proxies = new WeakMap();

/**
 * Default settings
 * @type {{}}
 * @private
 */
const defaultSettings = {
    context: ContextEnum.SVG,
    appearance: {
        stroke: true,
        strokeWidth: 2,
    },
    colors: {
        palette: ['#FF9400', '#05D1FF', '#719ED6', '#C9FF66', '#ADA8AA'],
        background: '#ffffff',
        text: '#000000',
        stroke: '#000000'
    },
    font: {
        autoScale: false,
        autoScaleFactor: 0.01,
        size: 20,
        sizeMin: 14,
        sizeMax: 30,
        family: 'Arial',
    },
    pie: {
        radius: 0.7,
        shift: 0,
        textFormatter: (index, label, sourceValue, computedValue, totalValue) => {
            return `${computedValue}%`;
        }
    },
    donut: {
        radius: 0.7,
        radiusDivider: 3,
        shift: 0,
        textFormatter: (index, label, sourceValue, computedValue, totalValue) => {
            return `${computedValue}%`;
        }
    },
    bar: {
        marginX: 0.1,
        marginY: 0.1,
        frameWidth: 0.8,
        barWidth: 0.8,
        labelSize: null,
        frameLabelSize: null,
        labelOutside: true,
        labelAbove: true,
        textFormatter: (index, label, sourceValue, computedValue, totalValue) => {
            return `${sourceValue}`
        },
        frames: {
            labelSize: null,
            labels: null,
            labelAbove: false,
            labelBottom: true,
            textFormatter: (index, label) => {
                return `${label}`
            }
        }
    },
    line: {
        marginX: 0.15,
        marginY: 0.15,
        lineWidth: 2,
        points: true,
        pointsSize: 4,
        labelSize: 10,
        labelAbove: true,
        textFormatter: (index, label, sourceValue) => {
            return `${sourceValue}`
        },
        frames: {
            labelSize:  10,
            labelAbove: true,
            labelBottom: true,
            textFormatter: (index, label) => {
                return `${label}`
            }
        },
    },
    legend: {
        show: true,
        position: 'left',
        textFormatter: (index, label, sourceValue, computedValue, totalValue) => {
            return `${label}`;
        }
    },
    grid: {
        show: true,
        lineWidth: 1,
        color: '#aaa',
        axisY: {
            tickSize: 30,
            maxTicks: null,
            textFormatter: (label) => {
                return `${label}`
            }
        }
    },
};

/**
 * EventHandlers container
 * @type {WeakMap<object,EventsHandler>}
 * @private
 */
const _eventHandlers = new WeakMap();

/**
 * Events definitions
 * @type {{}}
 */
const events = {
    changed: Symbol('changed'),
};

/**
 * SettingsManager class
 * Handles settings provided to Chart via simple object literal
 */
export default class SettingsManager extends EventsMixin(BaseClass) {
    /**
     * Create new SettingsManager instance
     * @param {{}} [settings={}] - settings
     */
    constructor(settings = {}) {
        super();

        // Check if settings is type of literal object
        if (settings.constructor !== {}.constructor) {
            throw new NotLiteralObjectError(settings);
        }

        // Create settings object
        _settings.set(this, DeepAssign.assign(settings, defaultSettings));

        // Create proxy
        _proxies.set(this, new Proxy(_settings.get(this), {
            set: (object, property, value) => {
                object[property] = value;

                // trigger event
                _eventHandlers.get(this).trigger(events.changed, {property, value});
                return true;
            }
        }));

        // Initialize events
        _eventHandlers.set(this, this.initEvents(events));
    }

    /**
     * Get settings via proxy (for reactivity)
     * @param {bool} [proxy=true] By default return proxy instead
     * @returns {Proxy}
     */
    getSettings(proxy = true) {
        return proxy ? _proxies.get(this) : _settings.get(this);
    }

    /**
     * Get settings via property
     * @returns {Proxy}
     */
    get settings() {
        return this.getSettings();
    }

    /**
     * Get default settings
     * @returns {Object}
     */
    static getDefault() {
        return Object.assign({}, defaultSettings);
    }

    /**
     * Get default settings via instance
     * @returns {Object}
     */
    get default() {
        return SettingsManager.getDefault();
    }
}
